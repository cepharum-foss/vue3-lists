/* eslint-env node */
require( "@rushstack/eslint-patch/modern-module-resolution" );

module.exports = {
	root: true,
	extends: [
		"plugin:vue/vue3-essential",
		"eslint-config-cepharum"
	],
	parserOptions: {
		ecmaVersion: "latest"
	},
	env: {
		browser: true
	},
	ignorePatterns: [
		"*.d.ts",
		"*.md",
		"!*.vue",
	],
	rules: {
		"vue/multi-word-component-names": "off",
		"vue/no-deprecated-slot-attribute": "warn"
	}
};
