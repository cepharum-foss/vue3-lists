import { isRef } from "vue";

/**
 * Validates some object looking like a list manager instance.
 *
 * @param {object} manager object to inspect
 * @returns {boolean} true if object seems to be a suitable list manager, false otherwise
 */
export function isValidListManager( manager ) {
	if ( !manager || typeof manager !== "object" ) {
		console.error( "[vue3-lists] list manager is missing" );
		return false;
	}

	if ( !isRef( manager.items ) ) {
		console.error( "[vue3-lists] list manager does not have reactive indicator for items to list" );
		return false;
	}

	if ( !isRef( manager.count ) ) {
		console.error( "[vue3-lists] list manager does not have reactive total number of items" );
		return false;
	}

	if ( !manager.schema || typeof manager.schema !== "object" ) {
		console.error( "[vue3-lists] list manager does not provide schema" );
		return false;
	}

	if ( !manager.modelSingular || !manager.modelPlural ) {
		console.error( "[vue3-lists] list manager does not provide names or i18n keys for singular/plural name of model" );
		return false;
	}

	if ( typeof manager.query !== "function" ) {
		console.error( "[vue3-lists] list manager does not provide query() method" );
		return false;
	}

	if ( typeof manager.create !== "function" ) {
		console.error( "[vue3-lists] list manager does not provide create() method" );
		return false;
	}

	if ( typeof manager.read !== "function" ) {
		console.error( "[vue3-lists] list manager does not provide read() method" );
		return false;
	}

	if ( typeof manager.update !== "function" ) {
		console.error( "[vue3-lists] list manager does not provide update() method" );
		return false;
	}

	if ( typeof manager.delete !== "function" ) {
		console.error( "[vue3-lists] list manager does not provide delete() method" );
		return false;
	}

	return true;
}

/**
 * Converts some consumer-provided set of actions into a set of properly defined
 * actions.
 *
 * @this {Cepharum.Vue3Lists.Item|Cepharum.Vue3Lists.Item[]}
 * @param {ActionsSetOrFunction} actions set of actions to normalize
 * @param {number} localIndex index of related item on current page of list
 * @param {number} localCount total number of items on current page of list
 * @param {number} index index of related item in list
 * @param {number} count total number of items in list
 * @returns {Array<ActionObject>} listed of normalized action definitions
 */
export function normalizeActions( actions, localIndex = undefined, localCount = undefined, index = undefined, count = undefined ) {
	const result = [];

	if ( typeof actions === "function" ) {
		actions = actions.call( this, index, count, localIndex, localCount );
	}

	if ( actions ) {
		const raw = typeof actions === "string" ? actions.trim().split( /\s*,\s*/ ).filter( i => i.trim() ) : actions;
		const isArray = Array.isArray( raw );
		const pool = isArray ? raw : Object.keys( raw || {} );

		for ( const iter of pool ) {
			const name = isArray ? iter?.name ?? iter : iter;
			const action = isArray ? iter : raw[iter];

			if ( typeof action === "string" ) {
				result.push( {
					name: name.replace( /^!/, "" ),
					icon: action,
					disabled: name.startsWith( "!" ),
				} );
			} else if ( action && typeof action === "object" ) {
				result.push( {
					name: name.replace( /^!/, "" ),
					disabled: name.startsWith( "!" ),
					...action
				} );
			}
		}
	}

	return result;
}

/**
 * Deeply clones some provided value.
 *
 * @param {any} source value to be cloned
 * @returns {any} clone of provided value
 */
export function clone( source ) {
	if ( source && typeof source === "object" ) {
		if ( typeof source.clone === "function" ) {
			return source.clone();
		}

		if ( Array.isArray( source ) ) {
			return source.map( item => clone( item ) );
		}

		const copy = {};

		for ( const [ key, value ] of Object.entries( source ) ) {
			switch ( key ) {
				case "constructor" :
				case "__proto__" :
				case "prototype" :
					break;

				default :
					copy[key] = clone( value );
			}
		}

		return copy;
	}

	return source ?? undefined;
}

/**
 * Deeply merges source object into target object.
 *
 * @param {Object} target target object to be updated by merging source
 * @param {Object} source source object to be deeply merged into target
 * @returns {Object} provided target or clone of provided source if target is nullish, not an object or incompatible with source by type of (array vs. object)
 */
export function merge( target, source ) {
	if ( target == null ) {
		return clone( source );
	}

	if ( source == null ) {
		// support unsetting target if source is null
		return source === undefined ? target : undefined;
	}

	if ( typeof target !== "object" ) {
		return clone( source );
	}

	if ( typeof source !== "object" ) {
		return source;
	}

	if ( Array.isArray( target ) ^ Array.isArray( source ) ) {
		return clone( source );
	}

	if ( Array.isArray( target ) ) {
		for ( let i = 0; i < source.length; i++ ) {
			target.push( merge( target[i], source[i] ) );
		}
	} else {
		for ( const [ key, value ] of Object.entries( source ) ) {
			switch ( key ) {
				case "constructor" :
				case "__proto__" :
				case "prototype" :
					break;

				default :
					target[key] = merge( value );
			}
		}
	}

	return target;
}

/**
 * Deeply compares two provided values.
 *
 * If parameter `strict` is true,
 *
 *  - scalar values are considered different even though they represent the same value using different types
 *  - falsy values such as empty string, false or 0 are considered different from each other and from nullish values
 *
 * @param {any} a first value to be compare
 * @param {any} b second value to compare first one with
 * @param {boolean} strict if true, scalars representing same value with different types are still considered different
 * @returns {boolean} true if both values are equal
 */
export function compare( a, b, strict = true ) {
	if ( a === b ) {
		return true;
	}

	if ( a == null && b == null ) {
		return true;
	}

	if ( a == null || b == null ) {
		return !strict && String( a ?? "" ) === String( b ?? "" );
	}

	if ( isNaN( a ) !== isNaN( b ) ) {
		return false;
	}

	if ( typeof a !== typeof b ) {
		if ( strict ) {
			return false;
		}

		switch ( typeof a ) {
			case "object" :
			case "function" :
				return false;

			default :
				return ( !a || !b ) && Boolean( a ) === Boolean( b );
		}
	}

	if ( typeof a === "function" ) {
		// don't compare functions at all, thus consider them equal to prevent them affecting actual result
		return true;
	}

	if ( typeof a === "object" ) {
		if ( a.constructor !== b.constructor ) {
			return false;
		}

		if ( a instanceof Map ) {
			if ( a.size !== b.size ) {
				return false;
			}

			for ( const [ key, value ] of a.entries() ) {
				if ( !b.has( key ) ) {
					return false;
				}

				if ( !compare( value, b.get( key ) ) ) {
					return false;
				}
			}

			for ( const key of b.keys() ) {
				if ( !a.has( key ) ) {
					return false;
				}
			}

			return true;
		}

		if ( a instanceof Set ) {
			if ( a.size !== b.size ) {
				return false;
			}

			for ( const value of a.values() ) {
				if ( !b.contains( value ) ) {
					return false;
				}
			}

			for ( const value of b.values() ) {
				if ( !a.contains( value ) ) {
					return false;
				}
			}

			return true;
		}

		if ( Array.isArray( a ) ) {
			if ( a.length !== b.length ) {
				return false;
			}

			for ( let i = 0, l = a.length; i < l; i++ ) {
				if ( !compare( a[i], b[i] ) ) {
					return false;
				}
			}

			return true;
		}

		for ( const key of Object.keys( a ) ) {
			if ( !Object.prototype.hasOwnProperty.call( b, key ) ) {
				return false;
			}

			if ( !compare( a[key], b[key] ) ) {
				return false;
			}
		}

		for ( const key of Object.keys( b ) ) {
			if ( !Object.prototype.hasOwnProperty.call( a, key ) ) {
				return false;
			}
		}

		return true;
	}

	return !strict && String( a ) === String( b );
}
