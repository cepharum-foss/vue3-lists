import { describe, it } from "vitest";
import "should";

import { compare, normalizeActions } from "./helper";

describe( "Helper function compare()", () => {
	it( "considers null and null equal", () => {
		compare( null, null ).should.be.true();
	} );

	it( "considers undefined and undefined equal", () => {
		compare( undefined, undefined ).should.be.true();
	} );

	it( "considers null and undefined equal", () => {
		compare( undefined, null ).should.be.true();
		compare( null, undefined ).should.be.true();
	} );
} );

describe( "Helper function normalizeActions()", () => {
	it( "is a function", () => {
		normalizeActions.should.be.Function();
	} );

	it( "can be invoked without any argument", () => {
		( () => normalizeActions() ).should.not.throw();
	} );

	it( "returns empty list when invoked without any argument", () => {
		normalizeActions().should.be.Array().which.is.empty();
	} );

	it( "accepts comma-separated names of custom actions as string", () => {
		const a = normalizeActions( "foo,bar , bam-boom " );

		a.should.be.Array().which.has.length( 3 );

		a[0].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[0].name.should.equal( "foo" );
		a[0].icon.should.equal( "foo" );

		a[1].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[1].name.should.equal( "bar" );
		a[1].icon.should.equal( "bar" );

		a[2].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[2].name.should.equal( "bam-boom" );
		a[2].icon.should.equal( "bam-boom" );
	} );

	it( "accepts list of custom action names as array of strings", () => {
		const a = normalizeActions( [ "foo", "bar", "bam-boom" ] );

		a.should.be.Array().which.has.length( 3 );

		a[0].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[0].name.should.equal( "foo" );
		a[0].icon.should.equal( "foo" );

		a[1].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[1].name.should.equal( "bar" );
		a[1].icon.should.equal( "bar" );

		a[2].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[2].name.should.equal( "bam-boom" );
		a[2].icon.should.equal( "bam-boom" );
	} );

	it( "accepts list of custom action descriptors as array of objects", () => {
		const a = normalizeActions( [ {
			name: "foo",
			icon: "caret-up"
		}, {
			name: "bar",
			icon: "caret-down"
		}, {
			name: "bam-boom",
			icon: "caret-left"
		} ] );

		a.should.be.Array().which.has.length( 3 );

		a[0].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[0].name.should.equal( "foo" );
		a[0].icon.should.equal( "caret-up" );

		a[1].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[1].name.should.equal( "bar" );
		a[1].icon.should.equal( "caret-down" );

		a[2].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[2].name.should.equal( "bam-boom" );
		a[2].icon.should.equal( "caret-left" );
	} );

	it( "accepts set of custom actions as object mapping their names into their icons' names", () => {
		const a = normalizeActions( {
			foo: "caret-up",
			bar: "caret-down",
			"bam-boom": "caret-left",
		} );

		a.should.be.Array().which.has.length( 3 );

		a[0].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[0].name.should.equal( "foo" );
		a[0].icon.should.equal( "caret-up" );

		a[1].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[1].name.should.equal( "bar" );
		a[1].icon.should.equal( "caret-down" );

		a[2].should.be.Object().which.has.properties( "name", "icon" ).and.not.has.property( "index" );
		a[2].name.should.equal( "bam-boom" );
		a[2].icon.should.equal( "caret-left" );
	} );

	it( "accepts set of custom actions as object mapping their names into proper action descriptors", () => {
		const a = normalizeActions( {
			foo: {
				icon: "caret-up",
				foo: 3,
				index: 50,
			},
			bar: {
				icon: "caret-down",
				foo: 2,
				index: 60,
				name: "alt-bar"
			},
			"bam-boom": {
				icon: "caret-left",
				foo: 1,
				index: 70,
			},
		} );

		a.should.be.Array().which.has.length( 3 );

		a[0].should.be.Object().which.has.properties( "name", "icon", "index", "foo" );
		a[0].name.should.equal( "foo" );
		a[0].icon.should.equal( "caret-up" );
		a[0].index.should.equal( 50 );
		a[0].foo.should.equal( 3 );

		a[1].should.be.Object().which.has.properties( "name", "icon", "index", "foo" );
		a[1].name.should.equal( "alt-bar" );
		a[1].icon.should.equal( "caret-down" );
		a[1].index.should.equal( 60 );
		a[1].foo.should.equal( 2 );

		a[2].should.be.Object().which.has.properties( "name", "icon", "index", "foo" );
		a[2].name.should.equal( "bam-boom" );
		a[2].icon.should.equal( "caret-left" );
		a[2].index.should.equal( 70 );
		a[2].foo.should.equal( 1 );
	} );
} );
