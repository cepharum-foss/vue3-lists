import { useL10n } from "@cepharum/vue3-i18n";

let imported;

/**
 * Sets up this library in context of a consuming project by importing
 * potentially used Shoelace components and by setting up loader for fetching
 * required translations of this library.
 *
 * This method can be invoked multiple times. However, setup is performed on
 * first invocation, only.
 *
 * @returns {Promise<void>} promise for having set up the library
 */
export function setup() {
	if ( !imported ) {
		const isSSR = typeof document === "undefined";

		imported = Promise.all( isSSR ? [] : [
			// have to list all imports explicitly here so that vite is capable of detecting them as dependencies
			import( "@shoelace-style/shoelace/dist/components/alert/alert.js" ),
			import( "@shoelace-style/shoelace/dist/components/button/button.js" ),
			import( "@shoelace-style/shoelace/dist/components/checkbox/checkbox.js" ),
			import( "@shoelace-style/shoelace/dist/components/dialog/dialog.js" ),
			import( "@shoelace-style/shoelace/dist/components/icon/icon.js" ),
			import( "@shoelace-style/shoelace/dist/components/icon-button/icon-button.js" ),
			import( "@shoelace-style/shoelace/dist/components/input/input.js" ),
			import( "@shoelace-style/shoelace/dist/components/option/option.js" ),
			import( "@shoelace-style/shoelace/dist/components/popup/popup.js" ),
			import( "@shoelace-style/shoelace/dist/components/select/select.js" ),
			import( "@shoelace-style/shoelace/dist/components/spinner/spinner.js" ),
			import( "@shoelace-style/shoelace/dist/components/tooltip/tooltip.js" ),
		] );

		const l10n = useL10n();

		l10n.setNamespaceLoader( "@vue3-lists", locale => import( `../l10n/${locale}.json` ) );
	}

	return imported;
}
