import {Component, ComputedRef, Ref} from "vue";

declare namespace Cepharum.Vue3Lists {
	export type MaybeRef<T> = T | Ref<T> | ComputedRef<T>;

	export interface ItemProperties<T> {
		[property: string]: any;
	}

	/**
	 * Represents single item of list.
	 */
	export interface Item<T> extends ItemProperties<T> {
		/**
		 * Uniquely identifies this item in its collection of items.
		 */
		uuid: string | number;

		/**
		 * Synchronously converts current item to string describing it e.g. for
		 * on-screen display.
		 */
		toString(): string;

		/**
		 * Converts current item to string describing it e.g. for on-screen
		 * display.
		 *
		 * This converter is optional. It's preferred over the common toString()
		 * when available. In addition, this converter can return a reactive
		 * reference to the resulting label and thus can deliver the label
		 * asynchronously.
		 */
		getLabel?(): Ref<string>;
	}

	export interface FilterSet<T> {
		[property: string]: any;
	}

	export interface QueryOptions {
		sortBy?: string;
		sortAscending?: boolean;
		offset?: number;
		limit?: number;
	}

	export type FieldTypes = "select" | "text" | "checkbox" | string;

	export type CellAlignment = "left" | "center" | "right";

	export interface PropertyDefinition {
		/**
		 * Names type of field presented in an editor.
		 *
		 * This property is used as fallback if `fieldType` is missing or falsy.
		 */
		type?: FieldTypes;

		/**
		 * Names type of field presented in an editor.
		 *
		 * This property is used in favor of `type` which might have different,
		 * backend-specific type name.
		 */
		fieldType?: FieldTypes;

		/**
		 * Names or selects custom component to use for presenting this field in
		 * editor.
		 */
		component?: string | Component;

		/**
		 * Provides custom classes to additionally apply e.g. to editor field.
		 */
		class: string | Array<string>;

		/**
		 * Selects alignment of values in cells representing this property in
		 * lists.
		 */
		alignment: CellAlignment;

		/**
		 * Provides text or its i18n key to show as label of field in editor.
		 */
		label?: string;

		/**
		 * Provides text or its i18n key to show as help text next to the field
		 * in editor.
		 *
		 * Not supported by all field types.
		 */
		hint?: string;

		/**
		 * If true, the field's value is presented in an editor, but it can't be
		 * changed.
		 *
		 * Not supported by all field types.
		 */
		readonly?: boolean;

		/**
		 * Selects number of columns in a 12-column grid this field is spanning.
		 *
		 * In a flexible layout, this value is used for orientation, only.
		 * Fields grow up to full width e.g. to support responsive designs. The
		 * width given here serves as a kind-of minimum width.
		 */
		width?: number;

		/**
		 * If true, the field should appear at the beginning of another row in
		 * editor.
		 */
		breakBefore?: boolean;

		/**
		 * If true, a field should be provided for editing the property of same
		 * name.
		 *
		 * If at least one property/field has a truthy value in this option,
		 * only those properties with a truthy `editable` option are rendered.
		 * Otherwise, a field is rendered for every defined property but those
		 * with a truthy `hidden` option.
		 */
		editable?: boolean;

		/**
		 * If true, no field may be rendered for this property.
		 */
		hidden?: boolean;

		/**
		 * Provides custom callback for validating a provided value as candidate
		 * for this field.
		 *
		 * The return value is an object of error messages mapping into true.
		 * This is used to merge custom validation results with results of
		 * internal validation without causing duplicates. Eventually, the keys
		 * of resulting object are used as error messages to show.
		 *
		 * If callback returns empty object (or no object), the given value is
		 * considered valid. If callback throws, the exception is logged and
		 * some generic validation issue with the given value is assumed.
		 *
		 * @param value value to be assessed
		 * @returns object mapping error messages or their i18n keys into boolean true
		 */
		validate?(value: any): { [error: string]: true };

		/**
		 * If true, the field requires some input.
		 */
		required?: boolean;

		/**
		 * Lists options to choose from in a selection-based editor control.
		 *
		 * The value must be given as list of strings or as map of values into
		 * option labels or their i18n keys. Regular objects are not supported
		 * due to their lack of consistent order of properties.
		 *
		 * For use with fields of type: select
		 */
		options: string[] | Map<string, string>;

		/**
		 * If true, the field's value can be a set of values from a collection
		 * of options to choose from.
		 *
		 * This is most useful in context of a editor field presenting some
		 * selector or similar to choose options from.
		 *
		 * For use with fields of type: select
		 */
		multiple?: boolean;

		/**
		 * Sets lower boundary on range of supported numeric values in a numeric
		 * input.
		 *
		 * For use with fields of type: number
		 */
		min?: number;

		/**
		 * Sets upper boundary on range of supported numeric values in a numeric
		 * input.
		 *
		 * For use with fields of type: number
		 */
		max?: number;

		/**
		 * Sets minimum number of characters to be entered.
		 *
		 * For use with fields of type: text
		 */
		minLength?: number;

		/**
		 * Sets maximum number of characters to be entered.
		 *
		 * For use with fields of type: text
		 */
		maxLength?: number;

		/**
		 * Provides regular expression to be met by field values to be
		 * considered valid.
		 *
		 * For use with fields of type: text
		 */
		pattern?: string;
	}

	export interface Schema {
		[property: string]: PropertyDefinition;
	}

	/**
	 * Describes some action that can be triggered by user either in context of
	 * whole list or in context of a single item of that list.
	 *
	 *
	 */
	export interface ActionObject {
		name: string;
		icon: string;
		disabled?: boolean;
		index?: number;
	}

	export interface ActionsObject {
		[name: string]: string | ActionObject
	}

	export type ActionsSet = Array<string | ActionObject> | ActionsObject;

	export type ActionsSetOrFunction = ActionsSet | ( ( index?: number, count?: number ) => ActionsSet );

	/**
	 * Describes result of having created another item in data source.
	 */
	export interface CreateResult<T> {
		/**
		 * Provides reference on provided item.
		 */
		createdItem: Item<T>;

		/**
		 * If true, the provided item has been integrated with current set of
		 * `items`, too. Otherwise, query() will be triggered again to update
		 * the collection of items.
		 */
		collectionUpdated?: boolean;
	}

	/**
	 * Controls sorting order of items in a list e.q. to be queried.
	 */
	export interface SortingOptions {
		/**
		 * Names property per listed item to sort them by.
		 */
		sortBy?: string;

		/**
		 * Controls to sort items in ascending or descending order. Sorting is
		 * expected to be in ascending order unless this is set `false`
		 * explicitly.
		 */
		sortAscending?: boolean;
	}

	/**
	 * Describes API supported by generic list component to access and control
	 * presented data.
	 */
	export interface ListManager<T> {
		/**
		 * Names model accessible through manager in its singular form.
		 */
		modelSingular: String;

		/**
		 * Names model accessible through manager in its plural form.
		 */
		modelPlural: String;

		/**
		 * If true or omitted, the user may basically use the create action to
		 * add items.
		 */
		mayCreate?: boolean;

		/**
		 * If true or omitted, the user may basically use the edit action to
		 * adjust items.
		 */
		mayUpdate?: boolean;

		/**
		 * If true or omitted, the user may basically use the delete action to
		 * remove items.
		 */
		mayDelete?: boolean;

		/**
		 * Provides name of property to use on providing custom search queries
		 * of user. This is required to actually present a search field to the
		 * user.
		 */
		searchProperty?: string;

		/**
		 * Exposes latest set of items to be listed with paging considered.
		 */
		items: Ref<Array<Item<T>>>;

		/**
		 * Exposes number of items available without paging considered.
		 */
		count: Ref<Number>;

		relatedManagers: { [key: string]: ListManager<any> };

		/**
		 * Queries data source for items matching some filter and updates
		 * `items` accordingly.
		 *
		 * @param filters optional set of filters to be satisfied by all resulting items
		 * @param options additional options for customizing query
		 * @returns promise settled with true after items have been updated,
		 *          gets used to display some loading indicator, when settled
		 *          with false, query has finished but another query has been
		 *          sent meanwhile so items haven't been updated
		 */
		query(filters: FilterSet<T>, options?: QueryOptions): Promise<boolean>;

		/**
		 * Creates new item from provided properties in data source.
		 *
		 * @param properties named values of item to create
		 * @returns promise settled after item has been created in data source
		 */
		create(properties: ItemProperties<T>): Promise<CreateResult<T>>;

		/**
		 * Qualifies provided item by reading all its available properties from
		 * data source in preparation for presenting it in a viewer or editor.
		 *
		 * This method is used to support limited set of properties provided per
		 * item when fetching a collection of items in opposition to requiring
		 * all properties when showing single item in a viewer or editor.
		 *
		 * @param item reference on item e.g. as provided in collection of items
		 * @returns promise settled after item's properties have been fetched from data source
		 */
		read(item: Item<T>): Promise<Item<T>>;

		/**
		 * Replaces properties of item in data source.
		 *
		 * @param item reference on item e.g. as provided in collection of items
		 * @param properties named values of item properties to replace existing ones in data source
		 * @returns promise settled after item's properties have been updated in data source, settles with true if `items` has been updated, too
		 */
		update(item: Item<T>, properties: ItemProperties<T>): Promise<boolean>;

		/**
		 * Removes item from data source.
		 *
		 * @param items list of items to remove e.g. as given in `items`
		 * @returns list of items eventually removed from data source
		 */
		delete(items: Array<Item<T>>): Promise<Array<Item<T>>>;

		/**
		 * Describes supported properties per item. It is e.g. used to implement
		 * an editor.
		 */
		schema: Schema;

		/**
		 * Optionally requests some default sorting to use on querying items
		 * unless some custom sorting order has been provided there.
		 */
		defaultSorting?: SortingOptions;

		filters: Array<QueryFilter> | QueryFilterMap;
	}

	export interface QueryFilter {
		type?: "static";
		name: string;
		q: any;
	}

	export interface QueryFilterMap {
		[ propertyName: string ]: QueryFilterDescriptor | string;
	}

	export interface QueryFilterDescriptor {
		type?: "static";
		q: any;
	}

	/**
	 * Defines options supported by composable creating instance of list manager
	 * for accessing backend based on Hitchy's ODM REST manager.
	 */
	export interface HitchyOdemRestManagerOptions {
		/**
		 * If true, fetch schema from backend. This is also the default when
		 * omitting this option.
		 */
		schema?: boolean;

		/**
		 * Refers to class of items generated by resulting list manager. The
		 * class can be used to implement specific per-item behavior such as
		 * generating a label.
		 */
		itemClass?: any;

		/**
		 * Provides custom URL to use instead of implicitly selected one for
		 * fetching a list of items from backend. The selected endpoints has to
		 * work like Hitchy's endpoint for listing a model's items via REST.
		 */
		queryUrl?: string;

		/**
		 * Provides custom URL to use instead of implicitly selected one for
		 * fetching a single item from backend. The selected endpoints has to
		 * work like Hitchy's endpoint for fetching a model's item via REST.
		 *
		 * If provided URL contains placeholder ":uuid" it is replaced with
		 * selected item's UUID. Otherwise, a forward slash and the selected
		 * item's UUID is appended to the provided URL.
		 */
		readUrl?: string;
	}

	/**
	 * Defines API for conveniently composing list managers.
	 */
	export interface ManagerComposable {
		/**
		 * Creates list manager providing access on a server-side model
		 * implemented with Hitchy's ODM and exposed via its REST API.
		 *
		 * @param baseUrl URL of server-side model, e.q. "/api/account"
		 * @param options customizations for created list manager
		 */
		useHitchyOdemRestManager<T = any>( baseUrl: string, options?: HitchyOdemRestManagerOptions ): ListManager<T>;
	}

	/**
	 * Defines API provided by composable for use with a custom editor component
	 * to manage a set of fields, their values and their states with regard to
	 * being valid or not, to have changed recently and to have been touched by
	 * user recently.
	 */
	export interface EditorComposable {
		/**
		 * Maps names of fields to present as part of editor into either field's
		 * definition.
		 */
		fields: ComputedRef<{ [fieldName: string]: FieldDefinition }>;

		/**
		 * Lists names of fields to present in proper sorting order which is
		 * resulting from a schema-like definition of properties.
		 */
		fieldNames: ComputedRef<string[]>;

		/**
		 * Maps either field's name into its state.
		 */
		states: Ref<{ [fieldName: string]: FieldState }>;

		/**
		 * Indicates if at least one of the field's value has changed recently
		 * or not.
		 */
		hasChanged: ComputedRef<boolean>;

		/**
		 * Indicates whether a field's definition must have truthy property
		 * `editable` (if true) to be part of editor or must have a truthy
		 * property `hidden` (if false) to be excluded from editor.
		 */
		includeMarked: ComputedRef<boolean>;

		/**
		 * Indicates if all fields of editor are valid or not.
		 */
		isValid: ComputedRef<boolean>;

		/**
		 * Indicates if editor is configured to create a new record instead of
		 * adjusting an existing one.
		 */
		isNew: ComputedRef<boolean>;

		/**
		 * Resets state of named field marking it as untouched again. The
		 * provided value is used as new original value on detecting changes.
		 *
		 * @param fieldName name of field to reset
		 * @param value value of field to be considered its original value from now on
		 */
		resetState(fieldName: String, value?: any): void;

		/**
		 * Reads record from a data source and invokes reset state on all fields
		 * listed in `fieldNames`.
		 */
		read(): Promise<void>;

		/**
		 * Compiles record consisting of values of either field listed in
		 * `fieldNames` and writes it to some data source.
		 *
		 * This method does not reset either field's state implicitly.
		 */
		write(): Promise<void>;
	}

	/**
	 * Describes options supported by composable for common features of editor
	 * components.
	 */
	export interface EditorComposableOptions {
		/**
		 * Provides custom function invoked instead of a list manager's read
		 * function to read an identified record's properties from a datasource.
		 *
		 * @param id record containing information for uniquely identifying the record of properties to read
		 * @returns all properties of selected record for use in editor
		 */
		reader?(id: ListRecord): Promise<FullRecord>;

		/**
		 * Provides custom function invoked instead of a list manager's write
		 * function to write a record of properties back to a datasource.
		 *
		 * @param id record containing information for uniquely identifying the record of properties to update, nullish to create a new record
		 * @param data record of properties' values to write to datasource
		 */
		writer?(id: ListRecord | undefined | null, data: FullRecord): Promise<void>;

		/**
		 * Optionally provides a schema-like definition of properties to present
		 * fields for in a consuming editor. This option is used instead of a
		 * definition found inlist manager's schema.
		 */
		properties?: { [fieldName: string]: PropertyDefinition };
	}

	/**
	 * Defines a record of properties suitable to create a new record in
	 * datasource or to replace an existing record there.
	 */
	export type FullRecord = Object;

	/**
	 * Defines a partial record consisting of properties uniquely identifying
	 * the record in a datasource at least.
	 */
	export type ListRecord = Object;

	/**
	 * Defines options supported to define an editor's field.
	 */
	export interface FieldDefinition extends PropertyDefinition {
		/**
		 * Provides name of properties this field is related to.
		 */
		name: string;
	}

	/**
	 * Defines state of a single field in editor at runtime.
	 */
	export interface FieldState {
		/**
		 * Provides field's current value.
		 */
		value?: any;

		/**
		 * Lists validation errors of field. If empty, the field is valid.
		 */
		errors: Array<string | Error>;

		/**
		 * Indicates if field's current value is different from its original
		 * value.
		 */
		changed: boolean;

		/**
		 * Indicates if field has been focused by user at least once since last
		 * reset of state.
		 */
		touched: boolean;

		/**
		 * Defines version property used by embedding editor to indicate change
		 * of field's value without user input e.g. on loading it from data
		 * source or on resetting it to its initial.
		 *
		 * Fields should use version property to take snapshots of values for
		 * detecting changes afterward.
		 */
		version: number;
	}

	/**
	 * Defines API commonly provided for implementing component for an editor
	 * field.
	 */
	export interface FieldComposable {
		/**
		 * Implements common behavior of a field component for integrating it
		 * with the generic list's editor.
		 *
		 * @param field refers to HTML element of field in DOM, used for checking validity as natively supported by element
		 * @param properties exposes actual properties of component as returned by defineProps() macro
		 * @param emitFn exposes callback returned by defineEmits() macro
		 */
		useField(
			field: Ref<HTMLElement>,
			properties: { [propertyName: string]: any },
			emitFn: (eventName: string, ...args: any
			) => void): FieldComposableAPI;

		/**
		 * Provides definitions of common properties to be included with every
		 * field component.
		 */
		properties: FieldCommonProperties;

		/**
		 * Lists names of events to be commonly emitted by a field component.
		 */
		events: string[];
	}

	/**
	 * Defines properties commonly supported by components implementing an
	 * editor field.
	 */
	export interface FieldCommonProperties {
		/**
		 * Defines property for passing the field's schema information from
		 * editor to field component.
		 */
		schema: Object;

		/**
		 * Defines common property for two-way binding the field's value.
		 */
		modelValue: Object;

		/**
		 * Defines property for two-binding list of validation errors that is
		 * implicitly indicating validation status of field, too. Either error
		 * is an error message or its i18n key.
		 */
		errors: Object;

		/**
		 * Defines property for two-binding status if field has been adjusted by
		 * user input recently.
		 */
		touched: Object;

		/**
		 * Defines property for two-binding status if field's value is currently
		 * different from time of last change of version (@see version).
		 */
		changed: Object;

		/**
		 * Defines version property used by embedding editor to indicate change
		 * of field's value without user input e.g. on loading it from data
		 * source or on resetting it to its initial.
		 *
		 * Fields should use version property to take snapshots of values for
		 * detecting changes afterward.
		 */
		version: Object;
	}

	export interface FieldComposableAPI {
		/**
		 * Creates an internal snapshot of field's current value for detecting
		 * changes to the value afterward.
		 */
		snapshot(): void;

		/**
		 * Validates current value of field and updates set of field-related
		 * errors accordingly.
		 *
		 * This method gets integrated with field internally. You may use this
		 * function to trigger additional validations.
		 */
		validate(): void;

		/**
		 * Instantly responds to any change of field's value by intention. It
		 * is marking the field as touched and emits notification event on
		 * change of value as part of two-way binding it.
		 */
		onInput(): void;

		/**
		 * Provides definition of field.
		 */
		schema: FieldDefinition;

		/**
		 * Selects type of field which might be different from type of field's
		 * related property.
		 */
		fieldType: string;

		/**
		 * Lists options of field in case of a selector field.
		 */
		options: Array<NormalizedListOption>;

		/**
		 * Indicates if e.g. options for selected field are loaded currently or
		 * not.
		 */
		loading: Ref<boolean>;
	}

	export interface NormalizedListOption {
		value: string;
		label: string;

		[customPropertyName: string]: any;
	}

	/**
	 * Provides API describing a related item of another (relating) item with
	 * the latter expected to hold the ("foreign") key of the former.
	 */
	export interface ReactiveRelatedItem<T> {
		/**
		 * Provides unique ID of related item as found in a relating item.
		 */
		foreignKey: ComputedRef<string>;

		/**
		 * Exposes error encountered while trying to read related item.
		 */
		error: ComputedRef<Error>;

		/**
		 * Exposes fetched item selected by foreign key found in relating item.
		 */
		item: ComputedRef<Item<T>>;

		/**
		 * Exposes label of related item e.g. for display in tables listing the
		 * relating item.
		 */
		label: ComputedRef<string>;

		/**
		 * Indicates whether related item is currently fetched or not.
		 */
		loading: ComputedRef<boolean>;
	}

	/**
	 * Provides composable support for accessing items related to another item.
	 */
	export interface ItemComposable {
		/**
		 * Exposes related item of a relating item.
		 *
		 * @param item relating item, which is: an item with a property holding that related item's unique ID as "foreign key"
		 * @param itemManager the relating item's list manager instance
		 * @param referringProperty name of relating item's property holding the foreign key
		 * @returns information on given item's related item
		 */
		useRelated<relatedT, T = any>( item: MaybeRef<Item<T>>, itemManager: MaybeRef<ListManager<relatedT>>, referringProperty: MaybeRef<string> ): ReactiveRelatedItem<relatedT>;
	}
}
