import DraggableCard from "./components/atoms/DraggableCard.vue";
import DraggableCardsList from "./components/molecules/DraggableCardsList.vue";
import DropZone from "./components/atoms/DropZone.vue";

import GenericList from "./components/organisms/GenericList.vue";
import GenericListFieldsGrid from "./components/templates/GenericListFieldsGrid.vue";
import GenericListTextField from "./components/atoms/GenericListTextField.vue";
import GenericListSelectorField from "./components/atoms/GenericListSelectorField.vue";
import GenericListRelatedField from "./components/atoms/GenericListRelatedField.vue";
import GenericListActions from "./components/atoms/GenericListActions.vue";
import LoadingIndicator from "./components/atoms/LoadingIndicator.vue";

import { useHitchyOdemRestManager } from "./composables/hitchy-odem-rest-manager.js";
import { useRelated } from "./composables/related.js";
import * as FieldComposable from "./composables/field.js";
import * as EditorComposable from "./composables/editor.js";

import { setup } from "./lib/setup.js";

export {
	DraggableCard, DraggableCardsList, DropZone,

	GenericList,
	GenericListFieldsGrid,
	GenericListTextField,
	GenericListSelectorField,
	GenericListRelatedField,
	GenericListActions,

	LoadingIndicator,

	FieldComposable,
	EditorComposable,
	setup
};

export const ManagerComposable = {
	useHitchyOdemRestManager,
};

export const ItemComposable = {
	useRelated,
};
