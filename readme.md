# @cepharum/vue3-lists

A Vue3 library of components for listing data

## License

[MIT](LICENSE)

## Install

```sh
npm install -D @cepharum/vue3-lists
```

## Manual

https://cepharum-foss.gitlab.io/vue3-lists/
