import { defineConfig } from "vite";
import { fileURLToPath } from "node:url";
import vue from "@vitejs/plugin-vue";

export default defineConfig( {
	plugins: [vue( {
		template: {
			compilerOptions: {
				isCustomElement: tag => tag.startsWith( "sl-" )
			}
		}
	} )],
	build: {
		copyPublicDir: false,
		lib: {
			entry: fileURLToPath( new URL( "./index.js", import.meta.url ) ),
			name: "cepharum-vue3-lists",
			fileName: "vue3-lists",
		},
		rollupOptions: {
			external: [
				"vue",
				"@cepharum/vue3-i18n",
				/\bshoelace\b/
			],
			output: {
				globals: {
					vue: "Vue",
					"@cepharum/vue3-i18n": "cepharum-vue3-i18n"
				}
			}
		}
	},
	test: {
		globals: true,
		environment: "jsdom",
	}
} );
