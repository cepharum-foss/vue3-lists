<template>
	<sl-dialog class="editor" open @sl-request-close="onClose">
		<div slot="label">
			<L10n
				:i18n="currentItem ? '@vue3-lists.GENERIC_LIST.EDITOR.UPDATE.title' : '@vue3-lists.GENERIC_LIST.EDITOR.CREATE.title'"
				:data="modelName"/>
		</div>

		<form @submit.prevent="submit" ref="form">
			<component
				ref="content"
				:is="manager.schema?.options?.editorForm || GenericListEditorForm"
				:i18nPrefix="i18nPrefix"
				:manager="manager"
				:item="currentItem"
				@loading="pending = true"
				@loaded="onLoaded"
				@hasChanged="hasChanged = $event"
				@isValid="isValid = $event"
			>
				<sl-alert variant="danger" open>
					<p>Editor form component <strong>{{ manager.schema?.options?.editorForm }}</strong> has not been
						registered.</p>
				</sl-alert>
			</component>
		</form>

		<LoadingIndicator :active="pending"/>

		<sl-checkbox
			v-if="!currentItem"
			class="createMore"
			slot="footer"
			:checked="createMore"
			:disabled="pending"
			@sl-change="createMore = $event.target.checked"
		>
			<L10n i18n="@vue3-lists.GENERIC_LIST.EDITOR.CREATE.more" :data="modelName"/>
		</sl-checkbox>

		<sl-button
			slot="footer"
			variant="primary"
			:disabled="!isValid || pending"
			@click="onSubmitButton"
		>
			<L10n
				:i18n="currentItem ? hasChanged ? '@vue3-lists.GENERIC_LIST.EDITOR.UPDATE.submit' : '@vue3-lists.GENERIC_LIST.EDITOR.close' : '@vue3-lists.GENERIC_LIST.EDITOR.CREATE.submit'"
				:data="modelName"
			/>
		</sl-button>
	</sl-dialog>

	<sl-dialog
		class="prompt"
		:open="dropChangesPrompt"
		@sl-request-close="onCloseConfirmed"
		no-header
	>
		<L10n i18n="@vue3-lists.GENERIC_LIST.EDITOR.LOSS.message" markdown/>

		<sl-button
			slot="footer"
			@click="onCloseConfirmed"
		>
			<L10n i18n="@vue3-lists.GENERIC_LIST.EDITOR.LOSS.leave"/>
		</sl-button>

		<sl-button
			slot="footer"
			variant="primary"
			@click="dropChangesPrompt = false"
		>
			<L10n i18n="@vue3-lists.GENERIC_LIST.EDITOR.LOSS.stay"/>
		</sl-button>
	</sl-dialog>
</template>

<script setup>
import { inject, ref, toRef, watch } from "vue";
import { useL10n, L10n } from "@cepharum/vue3-i18n";

import LoadingIndicator from "../atoms/LoadingIndicator.vue";
import GenericListEditorForm from "./GenericListEditorForm.vue";

const props = defineProps( {
	// exposes manager API for interacting with the data source
	manager: {
		type: Object,
		required: true,
	},
	// addresses item to edit, if nullish, new item is created
	item: Object,
	i18nPrefix: {
		String,
		default: "EDITOR.",
	},
} );

const emit = defineEmits( [ "done", "cancelled" ] );

const l10n = useL10n();
const toast = inject( "toast", undefined );

const form = ref();
const content = ref();

const currentItem = ref();
const numWritten = ref( 0 );

const createMore = ref( false );
const pending = ref( false );
const dropChangesPrompt = ref( false );

const modelName = l10n.translate( {
	model: props.manager.modelSingular,
	models: props.manager.modelPlural,
} );

/**
 * Caches provided item to support its local replacement, too.
 */
watch( toRef( props.item ), now => {
	currentItem.value = now;
}, { immediate: true } );

/**
 * Caches latest report on whether editor content is valid or not.
 */
const isValid = ref( false );

/**
 * Caches latest report on whether editor content has changed or not.
 */
const hasChanged = ref( false );

const onLoaded = loadingError => {
	pending.value = false;

	if ( loadingError ) {
		emit( numWritten.value > 0 ? "done" : "cancelled" );
	}
};

/**
 * Triggers submission of editor's form.
 *
 * This is used to trigger validation of fields.
 *
 * @returns {void}
 */
function onSubmitButton() {
	form.value.dispatchEvent( new SubmitEvent( "submit", { cancelable: true } ) );
}

/**
 * Responds to user clicking button or pressing Enter for saving changes.
 *
 * @returns {Promise<void>} promises processing is done
 */
async function submit() {
	if ( props.item && !hasChanged.value ) {
		emit( "cancelled" );
		return;
	}

	pending.value = true;

	try {
		await content.value.write();
	} catch ( cause ) {
		let message;
		let level = "alert";

		if ( cause instanceof Error ) {
			console.error( cause );
		} else if ( typeof cause === "string" ) {
			message = cause;
			level = "warn";
		}

		if ( !message ) {
			message = props.item ? "@vue3-lists.GENERIC_LIST.EDITOR.UPDATE.failed" : "@vue3-lists.GENERIC_LIST.EDITOR.CREATE.failed";
		}

		toast?.[level]( message, modelName );
		return;
	} finally {
		pending.value = false;
	}

	toast?.success( props.item ? "@vue3-lists.GENERIC_LIST.EDITOR.UPDATE.success" : "@vue3-lists.GENERIC_LIST.EDITOR.CREATE.success", modelName );

	numWritten.value++;

	if ( createMore.value ) {
		// trigger change of current item without actually addressing any existing item
		currentItem.value = currentItem.value === undefined ? null : undefined;
	} else {
		emit( "done" );
	}
}

/**
 * Responds to user requesting to close the editor without saving.
 *
 * @param {CustomEvent} event event triggered by sl-dialog to close
 * @returns {void}
 */
function onClose( event ) {
	event.preventDefault();

	if ( event.detail.source !== "overlay" ) {
		if ( hasChanged.value ) {
			dropChangesPrompt.value = true;
		} else {
			onCloseConfirmed();
		}
	}
}

/**
 * Responds to user having confirmed loss of changes when cancelling editor.
 *
 * @param {CustomEvent} [event] event triggered by sl-dialog to close
 * @returns {void}
 */
function onCloseConfirmed( event ) {
	event?.preventDefault();

	if ( !event?.detail?.source ) {
		emit( numWritten.value > 0 ? "done" : "cancelled" );
	}
}
</script>

<style scoped lang="scss">
sl-dialog.editor {
	--width: 1280px;

	&::part(panel) {
		position: relative;
	}
}

form {
	position: relative;
	display: flex;
	flex-flow: column nowrap;
	align-items: stretch;
}

.fields {
	display: flex;
	flex-flow: row wrap;
	margin-left: calc(-1 * var(--body-spacing));
	margin-top: calc(-1 * var(--body-spacing));

	> * {
		margin-left: var(--body-spacing);
		margin-top: var(--body-spacing);

		&.w-8 {
			flex: 1 1 calc(8.333% - var(--body-spacing));
		}

		&.w-17 {
			flex: 1 1 calc(16.666% - var(--body-spacing));
		}

		&.w-25 {
			flex: 1 1 calc(25% - var(--body-spacing));
			min-width: 10em;
		}

		&.w-33 {
			flex: 1 1 calc(33.333% - var(--body-spacing));
			min-width: 10em;
		}

		&.w-42 {
			flex: 1 1 calc(41.666% - var(--body-spacing));
			min-width: 10em;
		}

		&.w-50 {
			flex: 1 1 calc(50% - var(--body-spacing));
			min-width: 15em;
		}

		&.w-58 {
			flex: 1 1 calc(58.333% - var(--body-spacing));
			min-width: 15em;
		}

		&.w-67 {
			flex: 1 1 calc(66.666% - var(--body-spacing));
			min-width: 15em;
		}

		&.w-75 {
			flex: 1 1 calc(75% - var(--body-spacing));
			min-width: 20em;
		}

		&.w-83 {
			flex: 1 1 calc(83.333% - var(--body-spacing));
			min-width: 20em;
		}

		&.w-92 {
			flex: 1 1 calc(91.666% - var(--body-spacing));
			min-width: 20em;
		}

		&.w-100 {
			flex: 1 1 calc(100% - var(--body-spacing));
			min-width: 20em;
		}
	}

	> hr {
		flex: 1 1 100%;
		height: 0;
		overflow: hidden;
		margin: 0;
		padding: 0;
		border: 0;
	}
}

.createMore {
	margin-inline-end: var(--sl-spacing-x-small);
	margin-bottom: var(--sl-spacing-x-small);
}
</style>
