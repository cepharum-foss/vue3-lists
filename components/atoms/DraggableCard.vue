<script setup>
import { computed, ref, toRefs, watch } from "vue";

const props = defineProps( {
	index: Number,
	data: {
		type: Object,
		required: true,
	},
	// indicates if any card of the list is currently dragged or not (might be a different one)
	dragging: Boolean,
	locked: Boolean,
	component: [ String, Object ],
	scrollArea: {
		type: Number,
		default: 100,
	},
	scrollSpeed: {
		type: Number,
		default: 250,
	},
} );

const emits = defineEmits( [ "card-dragging", "card-dragged" ] );

const { locked } = toRefs( props );

const draggable = ref( true );

watch( locked, now => {
	draggable.value = !now;
}, { immediate: true } );

const card = ref();

// Provides root element of current card in DOM if available.
const cardElement = computed( () => {
	if ( card.value ) {
		if ( typeof card.value.getBoundingClientRect === "function" ) {
			return card.value;
		}

		if ( typeof card.value.element?.getBoundingClientRect === "function" ) {
			return card.value.element;
		}
	}

	return null;
} );

// if true, card is floating due to being dragged currently
const floating = ref( false );

const floatingX = ref();
const floatingY = ref();

const relativeX = ref();
const relativeY = ref();

/* caches styling of spot left on dragging off this card */
const cardWidth = ref();
const cardHeight = ref();
const cardDisplay = ref();
const cardMarginLeft = ref();
const cardMarginRight = ref();
const cardMarginTop = ref();
const cardMarginBottom = ref();

const lastTarget = ref();

const touchStart = ref();

let scrollingFn;

/**
 * Controls automatic scrolling of some scrollable content including target of
 * given pointer event based on whether the event occurs close to the visible
 * edges of its target's closest scrollable container.
 *
 * @param {MouseEvent} position pointer event to monitor
 * @returns {void}
 */
const autoScrollContainer = position => {
	// find some scrollable container
	for ( let node = position.target; node?.parentNode; node = node.parentNode ) {
		if ( node.offsetHeight > node.parentNode.offsetHeight ) {
			const scrollable = node.parentNode;
			const viewport = scrollable.getBoundingClientRect();

			const scroll = { left: 0, top: 0 };

			if ( position.clientX < viewport.left + props.scrollArea ) {
				scroll.left = -1;
			} else if ( position.clientX > viewport.right - props.scrollArea ) {
				scroll.left = 1;
			}

			if ( position.clientY < viewport.top + props.scrollArea ) {
				scroll.top = -1;
			} else if ( position.clientY > viewport.bottom - props.scrollArea ) {
				scroll.top = 1;
			}

			if ( scroll.left || scroll.top ) {
				startScrolling( scrollable, scroll.left, scroll.top );
				return;
			}

			break;
		}
	}

	scrollingFn = null;
};

/**
 * Sets up callback continuously apply provided relative scrolling to given node
 * unless scrollingFn isn't reset or replaced.
 *
 * @param {Element} node HTML element to scroll
 * @param {number} left horizontal scrolling delta to apply, may be <1
 * @param {number} top vertical scrolling delta to apply, may be <1
 * @returns {void}
 */
const startScrolling = ( node, left, top ) => {
	let scale = 0;
	let previousTs = 0;

	scrollingFn = ts => {
		if ( previousTs ) {
			const step = props.scrollSpeed * ( ts - previousTs ) / 1000;
			const oldScale = Math.floor( scale );

			scale = scale + step;

			if ( Math.floor( scale ) !== oldScale ) {
				node.scrollBy( {
					left: left * scale,
					top: top * scale,
					behavior: "instant",
				} );

				scale = scale - Math.floor( scale );
			}
		}

		previousTs = ts;

		if ( scrollingFn ) {
			requestAnimationFrame( scrollingFn );
		}
	};

	scrollingFn();
};

/**
 * Responds to user clicking and holding current card in preparation for
 * dragging it.
 *
 * @param {MouseEvent} event pointer event representing click-and-hold action
 * @returns {void}
 */
const startDragging = event => {
	let position = event;

	if ( !draggable.value ) {
		return;
	}

	if ( event.touches?.length > 1 ) {
		// another touch has started

		// -> stop any running dragging
		if ( floating.value ) {
			floating.value = false; // causes stopDragging() to assume cancellation
			stopDragging( { changedTouches: [touchStart.value] } );
		}

		return;
	}

	stopMonitorMoving();

	if ( event.touches?.length === 1 ) {
		position = event.touches[0];

		touchStart.value = position;
	}

	event.preventDefault();
	event.stopPropagation();

	const box = cardElement.value.getBoundingClientRect();

	relativeX.value = box ? position.clientX - box.x : position.offsetX;
	relativeY.value = box ? position.clientY - box.y : position.offsetY;

	floatingX.value = String( position.clientX - relativeX.value ) + "px";
	floatingY.value = String( position.clientY - relativeY.value ) + "px";

	if ( typeof window !== "undefined" ) {
		const element = card.value?.element || card.value;
		const style = window.getComputedStyle( element );

		cardDisplay.value = style.display;
		cardMarginTop.value = style.marginTop;
		cardMarginLeft.value = style.marginLeft;
		cardMarginRight.value = style.marginRight;
		cardMarginBottom.value = style.marginBottom;

		cardWidth.value = element.offsetWidth + "px";
		cardHeight.value = element.offsetHeight + "px";
	}

	floating.value = true;
	emits( "card-dragging" );

	startMonitorMoving();
};

/**
 * Handles user moving pointer to drag current card.
 *
 * @param {MouseEvent} event pointer event describing current position of pointer while moving it
 * @returns {void}
 */
const keepDragging = event => {
	if ( floating.value ) {
		const position = touchStart.value ? event.touches[0] : event;

		floatingX.value = String( position.clientX - relativeX.value ) + "px";
		floatingY.value = String( position.clientY - relativeY.value ) + "px";

		const target = cardElement.value.ownerDocument.elementFromPoint( position.clientX, position.clientY );

		if ( target !== lastTarget.value ) {
			if ( lastTarget.value ) {
				lastTarget.value.dispatchEvent( new CustomEvent( "card-hovered" ) );
			}

			if ( target ) {
				target.dispatchEvent( new CustomEvent( "card-hovering", {
					detail: {
						cardIndex: props.index,
						cardHeight: cardHeight.value,
					}
				} ) );
			}

			lastTarget.value = target;
		}

		autoScrollContainer( position );
	}
};

/**
 * Responds to user releasing previously dragged card.
 *
 * @param {MouseEvent} event pointer event describing position where user stopped dragging this card
 * @returns {void}
 */
const stopDragging = event => {
	const position = touchStart.value ? event.changedTouches[0] : event;

	const targets = floating.value ? cardElement.value.ownerDocument.elementsFromPoint( position.clientX, position.clientY ) : [];

	stopMonitorMoving();
	floating.value = false;
	scrollingFn = null;
	touchStart.value = null;

	if ( lastTarget.value ) {
		lastTarget.value.dispatchEvent( new CustomEvent( "card-hovered" ) );
		lastTarget.value = null;
	}

	emits( "card-dragged" );

	if ( targets.length ) {
		const dropTarget = targets.find( target => target?.closest?.( ".drop-zone" ) );

		if ( dropTarget ) {
			dropTarget.dispatchEvent( new CustomEvent( "card-dropped", {
				detail: {
					index: props.index,
					data: props.data,
				},
			} ) );
		}
	}
};

/**
 * Sets up listeners monitoring the card while it's dragged and until it's
 * dropped.
 *
 * @returns {void}
 */
const startMonitorMoving = () => {
	if ( typeof window !== "undefined" ) {
		if ( touchStart.value ) {
			window.addEventListener( "touchmove", keepDragging, { capture: true, passive: true } );
			window.addEventListener( "touchend", stopDragging, { capture: true } );
		} else {
			window.addEventListener( "pointermove", keepDragging, { capture: true, passive: true } );
			window.addEventListener( "pointerup", stopDragging, { capture: true } );
		}
	}
};

/**
 * Releases listeners previously set up to monitor the card while it's dragged.
 *
 * @returns {void}
 */
const stopMonitorMoving = () => {
	if ( typeof window !== "undefined" ) {
		if ( touchStart.value ) {
			window.removeEventListener( "touchmove", keepDragging, { capture: true, passive: true } );
			window.removeEventListener( "touchend", stopDragging, { capture: true } );
		} else {
			window.removeEventListener( "pointermove", keepDragging, { capture: true, passive: true } );
			window.removeEventListener( "pointerup", stopDragging, { capture: true } );
		}
	}
};
</script>

<template>
	<div class="spot" :class="{floating, dragging}">
		<component
			v-if="data.cardComponent || component"
			:is="data.cardComponent || component"
			:data="data"
			ref="card"
			class="card"
			:class="{draggable: draggable || floating}"
			:draggable="draggable"
			:floating="floating"
			:dragging="dragging"
			@pointerdown="startDragging"
			@touchstart="startDragging"
			@draggable="draggable = true"
			@locked="draggable = false"
		></component>
		<div
			v-else
			ref="card"
			class="card card__simple"
			:class="{draggable: draggable || floating}"
			@pointerdown="startDragging"
			@touchstart="startDragging"
		>
			<label>{{ data.label }}</label>
		</div>
	</div>
</template>

<style scoped lang="scss">
.spot {
	z-index: 2;

	&:not(.dragging) > .draggable {
		cursor: move;
	}

	&.floating {
		display: v-bind( cardDisplay );
		margin-top: v-bind( cardMarginTop );
		margin-left: v-bind( cardMarginLeft );
		margin-right: v-bind( cardMarginRight );
		margin-bottom: v-bind( cardMarginBottom );
		width: v-bind( cardWidth );
		height: v-bind( cardHeight );

		box-sizing: border-box;
		background: var( --dc-spot-background, #00000005 );
		border: var( --dc-spot-border, 2px dashed #00000020 );
		border-radius: var( --dc-spot-border-radius, 0 );

		> .draggable {
			pointer-events: none;
			position: fixed;
			z-index: 10000;
			top: v-bind( floatingY );
			left: v-bind( floatingX );
			width: v-bind( cardWidth );
			height: v-bind( cardHeight );

			&.card__simple {
				box-shadow: 0 0 5px #00000040;
				opacity: 0.7;
			}
		}
	}
}

.card__simple {
	box-sizing: border-box;
	border: var( --dc-card-border, 1px solid #00000060 );
	padding: var( --dc-card-padding, 0.3em 0.5em );

	&:not(.draggable) {
		opacity: var( --dc-card-disabled-opacity, 0.5 );
	}

	label {
		display: block;
		cursor: inherit;
	}
}
</style>
