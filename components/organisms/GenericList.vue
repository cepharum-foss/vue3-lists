<template>
	<div class="generic-list">
		<div class="header">
			<slot name="header">
				<GenericListNavigator
					v-model:filter="selectedFilters"
					v-model:limit="limit"
					v-model:offset="offset"
					v-model:query="search"
					:filter-options="dynamicFilters"
					:manager="manager"
				/>
			</slot>
		</div>
		<div class="content">
			<component :is="container" v-if="hasItems" class="items">
				<component
					:is="header || GenericListHeader"
					v-model:sortAscending="sortAscending"
					v-model:sortBy="sortBy"
					:allSelected="selected.size === items.length"
					:columns="columns"
					:i18nPrefix="i18nHeaderPrefix"
					:manager="manager"
					:selectable="selectable"
					:may-select-all="!maxSelectable || ( items.length > 0 && items.length <= maxSelectable )"
					:someSelected="selected.size > 0"
					:table="table"
					@toggleAll="toggleAll"
				/>
				<component :is="bodyContainer" :class="bodyClasses">
					<component
						:is="item || GenericListRow"
						v-for="(i, index) in items"
						:key="i.uuid"
						:columns="columns"
						:item="i"
						:manager="manager"
						:selectable="selectable"
						:selected="selected.has( i )"
						:table="table"
						@select="select( i, false )"
						@toggle="select( i, true )"
					>
						<GenericListActions
							:actions="localActions( i, index )"
							:count="manager.count"
							:index="offset + index"
							:localIndex="index"
							:localCount="limit || manager.count"
							@trigger="onLocalAction( i, $event )"
						/>
					</component>
				</component>
			</component>
			<sl-alert v-else-if="!loading" class="empty" open variant="neutral">
				<sl-icon slot="icon" name="info-circle"/>
				<L10n :i18n="emptyMessage || '@vue3-lists.GENERIC_LIST.empty'"/>
			</sl-alert>
			<slot name="overlay"></slot>
			<GenericLoadingIndicator :active="loading"/>
		</div>
		<div class="footer">
			<slot name="footer">
				<GenericListActions
					:actions="globalActions"
					@trigger="onGlobalAction"
				/>
			</slot>
		</div>
		<component
			:is="editor || manager.schema?.options?.editor || GenericListEditor"
			v-if="edit"
			:i18nPrefix="i18nEditorPrefix"
			:item="edit[0]"
			:manager="manager"
			@cancelled="edit = null"
			@done="onItemUpdated"
		></component>
		<GenericListModalDelete
			v-if="remove"
			:items="remove"
			:manager="manager"
			@cancelled="remove = null"
			@removed="onItemsRemoved"
		></GenericListModalDelete>
	</div>
</template>

<script setup>
import { computed, ref, toRef, toRefs, unref, watchEffect } from "vue";
import { L10n } from "@cepharum/vue3-i18n";

import { isValidListManager, normalizeActions } from "../../lib/helper";
import GenericListActions from "../atoms/GenericListActions.vue";
import GenericLoadingIndicator from "../atoms/LoadingIndicator.vue";
import GenericListHeader from "../molecules/GenericListHeader.vue";
import GenericListRow from "../molecules/GenericListRow.vue";
import GenericListModalDelete from "../molecules/GenericListModalDelete.vue";
import GenericListEditor from "../molecules/GenericListModalEditor.vue";
import GenericListNavigator from "../molecules/GenericListNavigator.vue";

defineExpose( {
	onItemUpdated
} );

const props = defineProps( {
	// Provides API for accessing and controlling data presented in list.
	manager: {
		type: Object,
		required: true,
		validator: isValidListManager,
	},
	// Selects custom component to use for rendering list header.
	header: Object,
	// Selects custom component to use for rendering single list item.
	item: Object,
	// Selects custom component to use for rendering editor atop the list.
	editor: Object,
	// Provides custom message or i18n key to display if list is empty
	emptyMessage: String,
	// Controls whether selection of rows is possible or not.
	selectable: {
		type: Boolean,
		default: true,
	},
	readonly: Boolean,
	// Selects maximum number of items the user may select simultaneously.
	maxSelectable: {
		type: [ Number, String ],
		validator: value => !value || /^\s*\d*\s*$/.test( value ),
	},
	// Indicates if header and item components are wrapped in a <table> element or some <div>
	table: {
		type: Boolean,
		default: true,
	},
	// Names properties per item to show as columns of generic tabular list.
	// Custom components might ignore this.
	columns: [ String, Array ],

	// Provides prefix for qualifying i18n keys for labelling list headers based
	// on name of either column's property.
	i18nHeaderPrefix: String,

	// Provides prefix for qualifying i18n keys for labelling editors fields based
	// on name of either column's property.
	i18nEditorPrefix: String,

	// Describes custom global actions.
	actions: Object,
} );

const emit = defineEmits( [ "selected", "action" ] );

// --- local data ---

const items = toRef( props.manager.items );

const loading = ref( false );

const selected = ref( new Set() );

const edit = ref( null );

const remove = ref( null );

const sortBy = ref( null );

const sortAscending = ref( true );

const offset = ref( 0 );

const limit = ref( 10 );

const search = ref( "" );

const selectedFilters = ref( [] );

// --- locally derived data ---

const { manager, selectable, readonly: readonlyTable, table } = toRefs( props );

const filters = computed( () => {
	const given = manager.value.filters ?? [];

	if ( Array.isArray( given ) ) {
		return given;
	}

	if ( typeof given === "object" ) {
		return Object.entries( given ).map( ( [ name, filter ] ) => ( {
			name,
			q: filter.q || filter,
			type: filter.type
		} ) );
	}

	return [];
} );

const staticFilters = computed( () => filters.value.filter( filter => filter.type === "static" ) );
const dynamicFilters = computed( () => filters.value.filter( filter => filter.type !== "static" ) );

const activeFilters = computed( () => {
	return ( selectedFilters.value || [] )
		.map( i => dynamicFilters.value[i] )
		.concat( staticFilters.value );
} );

const container = computed( () => ( table.value ? "table" : "div" ) );
const bodyContainer = computed( () => ( table.value ? "tbody" : "div" ) );
const bodyClasses = computed( () => ( table.value ? {} : { body: true } ) );

const hasItems = computed( () => {
	return Array.isArray( items.value ) && items.value.length > 0;
} );

const globalActions = computed( () => {
	const actions = new Map( [
		[ "create", {
			name: "create",
			icon: "plus-circle",
			index: 10,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.create",
		} ],
		[ "update", {
			name: "update",
			icon: "pencil",
			disabled: selected.value?.size !== 1,
			index: 20,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.update",
		} ],
		[ "delete", {
			name: "delete",
			icon: "trash",
			disabled: !selected.value?.size,
			index: 30,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.delete",
		} ],
		[ "refresh", {
			name: "refresh",
			icon: "arrow-clockwise",
			index: 40,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.refresh",
		} ],
	] );

	if ( !selectable.value || readonlyTable.value ) {
		actions.delete( "update" );
		actions.delete( "delete" );
	}

	if ( readonlyTable.value ) {
		actions.delete( "create" );
	}

	if ( !props.manager.mayCreate && props.manager.mayCreate != null ) {
		actions.delete( "create" );
	}

	if ( !props.manager.mayUpdate && props.manager.mayUpdate != null ) {
		actions.delete( "update" );
	}

	if ( !props.manager.mayDelete && props.manager.mayDelete != null ) {
		actions.delete( "delete" );
	}

	// adding index to items without index to append them to the end
	let index = 0;
	for ( const action of normalizeActions.call( items.value, props.actions ) ) {
		index++;
		actions.set( action.name, { index: 50 + 10 * index, ...action, } );
	}

	return Array.from( actions.values() )
		.sort( ( { index: li, name: ln } = {}, { index: ri, name: rn } = {} ) => {
			return ( li == null ? ri == null ? 0 : 1 : ri == null ? -1 : li - ri ) ||
				ln.toLowerCase().localeCompare( rn.toLowerCase() );
		} );
} );

/**
 * Compiles actions for a given item.
 *
 * @param {object} item listed item actions are related to
 * @param {number} localItemIndex index of item on current page of list
 * @returns {{name: string, icon: string, tooltip: string, index: number}[]} list of item's actions
 */
const localActions = ( item, localItemIndex ) => {
	const actions = new Map( [
		[ "update", {
			name: "update",
			icon: "pencil",
			index: 20,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.updateItem",
		} ],
		[ "delete", {
			name: "delete",
			icon: "trash",
			index: 30,
			tooltip: "@vue3-lists.GENERIC_LIST.TOOLTIP.deleteItem",
		} ],
	] );

	if ( readonlyTable.value ) {
		actions.delete( "update" );
		actions.delete( "delete" );
	}

	if ( !props.manager.mayUpdate && props.manager.mayUpdate != null ) {
		actions.delete( "update" );
	}

	if ( !props.manager.mayDelete && props.manager.mayDelete != null ) {
		actions.delete( "delete" );
	}

	// adding index to items without index to append them to the end
	const numTotalItems = unref( props.manager.count );
	let index = 0;

	for ( const action of normalizeActions.call( item, item.actions, localItemIndex, numTotalItems, offset.value + localItemIndex, numTotalItems ) ) {
		index++;
		actions.set( action.name, { index: 40 + index * 10, ...action } );
	}

	return Array.from( actions.values() )
		.sort( ( { index: li, name: ln } = {}, { index: ri, name: rn } = {} ) => {
			return ( li == null ? ri == null ? 0 : 1 : ri == null ? -1 : li - ri ) ||
				ln.toLowerCase().localeCompare( rn.toLowerCase() );
		} );
};

// --- actions ---

/**
 * Adjusts selection of items.
 *
 * @param {Item} item refers to item of collection `items` that should be selected
 * @param {boolean} multiToggle if true, provided item's inclusion with set of selected items should be toggled without replacing any other selected item
 * @returns {void}
 */
function select( item, multiToggle ) {
	let changed = false;

	if ( !multiToggle ) {
		changed = selected.value.size > 0;
		selected.value.clear();
	}

	if ( selected.value.has( item ) ) {
		if ( multiToggle ) {
			changed = true;
			selected.value.delete( item );
		}
	} else if ( !props.maxSelectable || selected.value.size < props.maxSelectable ) {
		changed = true;
		selected.value.add( item );
	}

	if ( changed ) {
		emit( "selected", Array.from( selected.value ) );
	}
}

/**
 * Removes items from selection that aren't part of currently listed items
 * anymore.
 *
 * @returns {void}
 */
function syncSelection() {
	for ( const item of selected.value ) {
		const match = items.value.find( candidate => item.uuid === candidate.uuid );

		if ( match == null ) {
			selected.value.delete( item );
		} else if ( match !== item ) {
			selected.value.delete( item );
			selected.value.add( match );
		}
	}
}

/**
 * Fetches updated list of items to list from data source via provided manager.
 *
 * @returns {Promise<void>} promise for having updated collection of `items`
 */
async function query() {
	loading.value = true;

	try {
		const filter = {};

		if ( props.manager.searchProperty && String( search.value ?? "" ).trim() !== "" ) {
			filter[props.manager.searchProperty] = String( search.value ).trim();
		}

		if ( activeFilters.value && activeFilters.value.length > 0 ) {
			for ( const f of activeFilters.value ) {
				if ( f.name && f.q ) filter[f.name] = f.q;
			}
		}

		/** @type {QueryOptions} */
		const options = {
			offset: offset.value,
			limit: limit.value,
		};

		if ( sortBy.value != null ) {
			options.sortBy = sortBy.value;
			options.sortAscending = sortAscending.value !== false;
		}

		await props.manager.query( filter, options );
	} finally {
		loading.value = false;
	}

	syncSelection();
}

/**
 * Selects or unselects all currently listed items.
 *
 * @returns {void}
 */
function toggleAll() {
	if ( selected.value.size === items.value.length ) {
		selected.value.clear();
	} else {
		selected.value = new Set( items.value );
	}
}

/**
 * Performs global action.
 *
 * @param {Cepharum.Vue3Lists.ActionObject} action description of action to perform
 * @returns {void}
 */
function onGlobalAction( action ) {
	switch ( action.name ) {
		case "refresh" :
			onItemUpdated();
			break;

		case "create" :
			edit.value = [];
			break;

		case "update" :
			edit.value = Array.from( selected.value ).slice( 0, 1 );
			break;

		case "delete" :
			remove.value = Array.from( selected.value );
			break;

		default : {
			const context = Array.from( selected.value );
			const event = new CustomEvent( "action", { detail: { action, items: context } } );

			emit( "action", event, action, context );

			if ( !event.defaultPrevented ) {
				query();
			}
		}
	}
}

/**
 * Performs local action.
 *
 * @param {Item} item target of local action
 * @param {Cepharum.Vue3Lists.ActionObject} action description of action to perform on item
 * @returns {void}
 */
function onLocalAction( item, action ) {
	switch ( action.name ) {
		case "update" :
			edit.value = [item];
			break;

		case "delete" :
			remove.value = [item];
			break;

		default : {
			const event = new CustomEvent( "action", { detail: { action, items: [item] } } );

			emit( "action", event, action, [item] );

			if ( !event.defaultPrevented ) {
				query();
			}
		}
	}
}

/**
 * Post-processes list state after some item has been updated by embedded
 * editor.
 *
 * @returns {Promise<void>} promise for having fixed state of list e.g. by re-fetching items from data source
 */
async function onItemUpdated() {
	await query();

	syncSelection();

	edit.value = null;
}

/**
 * Post-processes list state after one or more items have been removed.
 *
 * @returns {Promise<void>} promise for having fixed state of list e.g. by re-fetching items from data source
 */
async function onItemsRemoved() {
	await query();

	remove.value = null;
}

watchEffect( query );

// --- life cycle hooks ---
</script>

<style lang="scss">
:root {
	--gl-color-text: var(--sl-color-neutral-1000);
	--gl-color-action: var(--sl-color-primary-700);
	--gl-color-action-hover: var(--sl-color-primary-500);

	--gl-cell-normal-color-bg: var(--sl-color-neutral-0);
	--gl-cell-normal-color-bg-alt: var(--sl-color-neutral-50);
	--gl-cell-normal-color-border: var(--sl-color-neutral-200);

	--gl-cell-hover-color-bg: var(--sl-color-primary-100);
	--gl-cell-hover-color-border: var(--sl-color-primary-200);

	--gl-cell-selected-color-text: var(--sl-color-neutral-0);
	--gl-cell-selected-color-action: var(--sl-color-primary-200);
	--gl-cell-selected-color-action-hover: var(--sl-color-neutral-0);
	--gl-cell-selected-color-bg: var(--sl-color-primary-400);
	--gl-cell-selected-color-border: var(--sl-color-primary-500);

	--gl-cell-header-color-text: var(--sl-color-primary-50);
	--gl-cell-header-color-action-hover: var(--sl-color-primary-50);
	--gl-cell-header-color-bg: var(--sl-color-primary-600);
	--gl-cell-header-color-border: var(--sl-color-primary-700);

	&.sl-theme-dark {
		--gl-color-action: var(--sl-color-primary-700);
		--gl-color-action-hover: var(--sl-color-primary-950);

		--gl-cell-selected-color-text: var(--sl-color-neutral-1000);
		--gl-cell-selected-color-action: var(--sl-color-primary-800);
		--gl-cell-selected-color-action-hover: var(--sl-color-neutral-1000);
	}
}
</style>

<style lang="scss" scoped>
.generic-list {
	display: flex;
	flex-flow: column nowrap;
	height: 100%;
	max-height: 100%;
	overflow: auto;

	---glc-text-fb: var(--gl-color-text);
	---glc-action-fb: var(--gl-color-action, var(---glc-text-fb));
	---glc-action-hover-fb: var(--gl-color-action-hover, var(---glc-action-fb));
	---glc-bg-fb: var(--gl-color-bg);
	---glc-border-fb: var(--gl-color-border);

	---glc-text: var(---glc-text-fb);
	---glc-action: var(---glc-action-fb);
	---glc-action-hover: var(---glc-action-hover-fb);
	---glc-bg: var(---glc-bg-fb);
	---glc-border: var(---glc-border-fb);

	---glc-selector-top: var(--gl-selector-padding-top, var(--gl-selector-padding-vertical, var(--gl-selector-padding, 15px)));
	---glc-selector-right: var(--gl-selector-padding-right, var(--gl-selector-padding-horizontal, var(--gl-selector-padding, 6px)));
	---glc-selector-bottom: var(--gl-selector-padding-bottom, var(--gl-selector-padding-vertical, var(--gl-selector-padding, 15px)));
	---glc-selector-left: var(--gl-selector-padding-left, var(--gl-selector-padding-horizontal, var(--gl-selector-padding, 15px)));

	> .header {
		flex: 1 1 0;
	}

	> .content {
		flex: 1000 1 1%;
		overflow: auto;

		> .items {
			display: table;
			border-spacing: 0;
			width: var(--gl-list-width, 1280px);
			max-width: 100%;

			> .body {
				display: table-row-group;
			}

			> :deep(tbody), > :deep(thead), > :deep(.body), > :deep(.head) {
				> tr, > .row {
					> .item-selector {
						width: 1px;

						> sl-checkbox {
							display: flex;

							&::part(base) {
								padding: var(---glc-selector-top) var(---glc-selector-right) var(---glc-selector-bottom) var(---glc-selector-left);
							}

							&::part(control) {
								justify-self: center;
								transition-duration: 0s;
							}
						}
					}
				}
			}

			> :deep(tbody), > :deep(.body) {
				> *:hover {
					position: relative;
					z-index: 20;
				}
			}
		}

		> .empty {
			font-style: italic;
			text-align: center;
			display: block;
			width: var(--gl-list-width, 1280px);
			max-width: 100%;
		}
	}

	> .footer {
		--gl-action-size: var(--gl-global-action-size, 2em);

		flex: 1 1 0;
		margin-top: calc(var(--gl-spacing, 20px) - var(--sl-spacing-x-small));
		display: flex;
		flex-flow: row nowrap;
		justify-content: space-around;
		align-items: center;
	}

	sl-alert.empty {
		&::part(base) {
			border-top-color: var(--sl-color-extra-700, var(--sl-color-primary-700));
		}

		&::part(icon) {
			color: var(--sl-color-extra-700, var(--sl-color-primary-700));
		}
	}
}
</style>
