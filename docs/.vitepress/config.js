import { viteStaticCopy } from "vite-plugin-static-copy";

export default {
	title: "Vue3 Lists",
	description: "A Vue3 component library for listing data",
	base: "/vue3-lists",
	outDir: "../public",
	themeConfig: {
		sidebar: [
			{ text: "Getting started", link: "/getting-started.md" },
			{ text: "Draggable Lists", items: [
				{ text: "DraggableCard", link: "/api/draggable-card.md" },
				{ text: "DraggableCardsList ⭐", link: "/api/draggable-cards-list.md" },
				{ text: "DropZone", link: "/api/drop-zone.md" },
			] },
			{ text: "Generic Lists", items: [
				{ text: "GenericList ⭐", link: "/api/generic-list.md" },
				{ text: "GenericListFieldsGrid", link: "/api/generic-list-fields-grid.md" },
				{ text: "GenericListTextField", link: "/api/generic-list-text-field.md" },
				{ text: "GenericListSelectorField", link: "/api/generic-list-selector-field.md" },
				{ text: "GenericListRelatedField", link: "/api/generic-list-related-field.md" },
				{ text: "GenericListActions", link: "/api/generic-list-actions.md" },
				{ text: "LoadingIndicator", link: "/api/loading-indicator.md" },
				{ text: "Composables", items: [
					{ text: "EditorComposable", link: "/api/editor-composable.md" },
					{ text: "FieldComposable", link: "/api/field-composable.md" },
					{ text: "ManagerComposable ⭐", link: "/api/manager-composable.md" },
				] },
			] },
		]
	},
	vue: {
		template: {
			compilerOptions: {
				isCustomElement: tag => tag.startsWith( "sl-" )
			}
		}
	},
	vite: {
		plugins: [
			viteStaticCopy( {
				targets: [
					{
						src: "../node_modules/@shoelace-style/shoelace/dist/assets/",
						dest: "shoelace/",
					}
				]
			} ),
		],
	},
};
