import DefaultTheme from "vitepress/theme";
import { defineAsyncComponent } from "vue";
import { useThemeSync } from "@cepharum/vue3-utils/dist/vue3-utils.js";

import "./style.scss";
import "../../../dist/style.css";

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path.js";
import "@shoelace-style/shoelace/dist/themes/light.css";
import "@shoelace-style/shoelace/dist/themes/dark.css";

setBasePath( import.meta.env.BASE_URL + "shoelace/" );

import { setup } from "../../../dist/vue3-lists.js";

const manualComponents = import.meta.glob( "./components/**/*.vue" );
const libraryComponents = import.meta.glob( "../../../components/**/*.vue" );

export default {
	extends: DefaultTheme,
	async enhanceApp( ctx ) {
		setBasePath( import.meta.env.BASE_URL + "shoelace/" );

		await setup( true );

		for ( const [ name, component ] of Object.entries( manualComponents ) ) {
			ctx.app.component( name.replace( /^.+\/|\.vue$/g, "" ), defineAsyncComponent( component ) );
		}

		for ( const [ name, component ] of Object.entries( libraryComponents ) ) {
			ctx.app.component( name.replace( /^.+\/|\.vue$/g, "" ), defineAsyncComponent( component ) );
		}

		useThemeSync();
	}
};
