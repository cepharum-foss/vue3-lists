# DropZone

The `<DropZone />` component is **used internally** by the [DraggableCardsList](draggable-cards-list.md) component to represent a gap between two listed cards available for dropping a dragged card. It is described here for understanding its features as part of a list of draggable cards.

In a **DraggableCardsList**, the **DropZone** is an additional gap between two cards. When dragging a card over it, the **DropZone** is animating growth of its gap to indicate acceptance for dropping that card there. 
