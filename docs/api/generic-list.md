---
outline:
  level: [ 2, 3 ]
---

# GenericList

:::tip About the ⭐
This library provides many components and composables, but only a few of them are necessary to get you started. In the
menu, we marked those with a ⭐.
:::

The `<GenericList>` component provides a feature-rich and commonly useful list of items presented in a tabular style. It
provides paging, filtering, editing items, item selections, global actions, actions per item and more. It supports
custom components for rendering special data and it works with a manager that is in charge of providing data to be
listed as well as responding to actions triggered by the user.

<DemoGenericList/>

## CSS properties

For custom styling, several CSS properties are used by the component:

| property                            | description                                                              |
|-------------------------------------|--------------------------------------------------------------------------|
| `--gl-color-text`                   | default color of text                                                    |
| `--gl-color-action`                 | default color for hovering controls                                      |
| `--gl-cell-normal-color-text`       | text color in cells                                                      |
| `--gl-cell-normal-color-action`     | color for hovering controls in cells                                     |
| `--gl-cell-normal-color-bg`         | color for cell backgrounds                                               |
| `--gl-cell-normal-color-border`     | color for cell borders                                                   |
| `--gl-cell-normal-color-text-alt`   | text color in cells of every second row                                  |
| `--gl-cell-normal-color-action-alt` | color for hovering controls in cells of every second row                 |
| `--gl-cell-normal-color-bg-alt`     | color for cell backgrounds in every second row                           |
| `--gl-cell-normal-color-border-alt` | color for cell borders in every second row                               |
| `--gl-cell-hover-color-text`        | text color in cells of row currently hovered by pointer                  |
| `--gl-cell-hover-color-action`      | color for hovering controls in cells of row currently hovered by pointer |
| `--gl-cell-hover-color-bg`          | color for cell backgrounds in cells of row currently hovered by pointer  |
| `--gl-cell-hover-color-border`      | color for cell borders in cells of row currently hovered by pointer      |
| `--gl-cell-selected-color-text`     | text color in cells of a selected row                                    |
| `--gl-cell-selected-color-action`   | color for hovering controls in cells of a selected row                   |
| `--gl-cell-selected-color-bg`       | color for cell backgrounds in cells of a selected row                    |
| `--gl-cell-selected-color-border`   | color for cell borders in cells of a selected row                        |
| `--gl-cell-header-color-text`       | text color in cells of header row                                        |
| `--gl-cell-header-color-action`     | color for hovering controls in cells of header row                       |
| `--gl-cell-header-color-bg`         | color for cell backgrounds in cells of header row                        |
| `--gl-cell-header-color-border`     | color for cell borders in cells of header row                            |

For styling, most of these are optional whenever they have more common fallbacks, e.g. the styling for alternating colors in every second row can be omitted for they fallback to use the normal coloring cells. The normal coloring of cell text is falling back to the default color of text when omitted.
