---
outline:
  level: [2, 3]
---

# GenericListActions

This component renders a collection of clickable icon buttons with optional tooltips triggering an action associated with either icon button on clicking.

:::code-group
```html [simple]
<GenericListActions
  actions="start,!stop,cancel"
></GenericListActions>
```

```html [as array]
<GenericListActions
  :actions="['start','!stop','cancel']"
></GenericListActions>
```

```html [as object]
<GenericListActions
  :actions="{
    start: 'play',
    stop: {
      icon: 'stop',
      disabled: true,
      index: 100,
    },
    cancel: {
      icon: 'x',
      name: 'cancel-action',
      index: 200,
    }
  }"
></GenericListActions>
```

```html [as function]
<GenericListActions
  :actions="myActionGeneratorFn"
  :item="listedItem"
></GenericListActions>
```
:::
## Properties

### actions

Lists actions to be represented as a collection of icon buttons. It accepts actions defined in multiple ways:

* Actions can be given as a comma-separated string of action names. 

  Either name is used for naming the related button's icon. It may be prepended with a single exclamation mark to disable related button.

* Actions can be given as an array of strings and/or objects.

  Either string is again used as name of action as well as name of related button's icon. And prepending it with an exclamation mark is disabling the resulting button. Either item of array can be an object separately providing 

   * an action's `name`,
   * the name of related button's `icon`,
   * an optional text or i18n key for a `tooltip` to show on user hovering the icon button with the pointer and
   * a boolean indicating whether the button is `disabled` or not. 
   * In addition, a numeric `index` can be provided to control order of actions.

* Actions can be given as object.

  Property names of this object are used as either action's name by default (if no action name is given in property's value explicitly). 

  Property values can be either a string naming related button's icon or an object consisting of properties `name`, `icon`, `disabled` and `index` as described for arrays of actions above.

* `actions` can be a function invoked with optionally provided index and count to generate the actual set of actions on the fly. The returned definition can be of any other format described here.

  On providing optional `item`, the function is invoked in its context.

### item

Optionally provides a context to function given in `actions` to be invoked for actually generating resulting set of actions.

### index

Actions are usually provided either globally or in context of a single item. In the latter case, this property is providing the item's zero-based index into the list of items so that actions might respond to e.g. being applied to first item of a list.

### count

This is the number of listed items and should be provided if `index` has been provided, too. It can be used by actions to respond to e.g. being applied to last item of a list.
