---
outline:
  level: [2, 3]
---

# DraggableCard

The `<DraggableCard />` component is **used internally** by the [DraggableCardsList](draggable-cards-list.md) component to represent a single item of list. It is described here for understanding its features as part of a list of draggable cards.

## Properties

### component

This optional property selects a component to use for actually displaying the card. It can be the name of a globally registered component or a reference to a locally available component. [`data`](#data) property is forwarded as such to that component.

### data

This mandatory reference exposes single record of [`DraggableCardsList`](draggable-cards-list.md)'s [`cards`](draggable-cards-list.md#cards) property to be represented by current card.

### dragging

This boolean property indicates whether this card is currently dragged by the user. This flag is controlled via events the card is emitted itself. It is also used to control the state of card in DOM so that its styling can depend on it.

### index

This mandatory numeric property exposes the card's 0-based index into the containing list of cards. It is used to properly emit events on dragging and dropping cards to reorder items.

### locked

This optional boolean property controls if the card is locked and thus not draggable.

### scrollArea

This optional numeric property controls width of area on either edge of list where a dragged card is scrolling the list. The value is given in pixels. Its default is `100`.

### scrollSpeed

This optional numeric property defines number of pixels to scroll per second. Its default is `250`.


## Events

### card-dragging

This event is emitted when the user has started dragging it.

### card-dragged

This event is emitted when the user has stopped dragging it.
