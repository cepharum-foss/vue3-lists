---
outline:
  level: [2, 3]
---

# DraggableCardsList

:::tip About the ⭐
This library provides many components and composables, but only a few of them are necessary to get you started. In the menu, we marked those with a ⭐.
:::

The `<DraggableCardsList />` component renders a list of cards based on a given set of data objects supporting rendered cards to be re-ordered using drag-n-drop.

## Simple Example

```html
<DraggableCardsList
    :cards="items"
    @move-card="moveCard"
/>
```

In this example, an array of objects is expected in variable `items` and provided for describing cards to list. All cards are draggable by default (but can be locked individually). 

On user dragging a card and dropping it in different position of this list, the Vue3 event `move-card` is emitted with the dragged card's old and new index into the provided array of objects as arguments. A handler could look like this:

```js
const moveCard = ( oldIndex, newIndex ) => {
    items.value.splice(
        newIndex - ( oldIndex < newIndex ? 1 : 0 ), 0, 
        ...items.value.splice( oldIndex, 1 ) 
    );
};
```

This could be the array of objects:

```js
const items = ref( [
  { uuid: "1", label: "Entry 1" },  
  { uuid: "2", label: "Entry 2" },  
  { uuid: "3", label: "Entry 3" },  
  { uuid: "4", label: "Entry 4" },  
  { uuid: "5", label: "Entry 5" },  
  { uuid: "6", label: "Entry 6", locked: true },  
  { uuid: "7", label: "Entry 7" },  
  { uuid: "8", label: "Entry 8" },  
  { uuid: "9", label: "Entry 9" },  
  { uuid: "10", label: "Entry 10" },  
] );
```

The resulting list looks like this:

<DemoDraggable mode="simple" />

## Advanced examples

You can name a custom component for rendering either card of list:

```html
<DraggableCardsList
  card-component="DraggableCardAlt"
  :drop-zone-size="20"
  :cards="items"
  @move-card="moveCard"
></DraggableCardsList>
```

A resulting example could look like this:

<DemoDraggable mode="advanced" />

Eventually, provided data can name different custom components per listed record:

```js
const items = ref( [
  { uuid: "1", label: "Entry 1" },  
  { uuid: "2", label: "Entry 2" },  
  { uuid: "3", label: "Entry 3", cardComponent: DraggableCardRounded },  
  { uuid: "4", label: "Entry 4" },  
  { uuid: "5", label: "Entry 5", cardComponent: "DraggableCardRotated" },  
  { uuid: "6", label: "Entry 6", locked: true },  
  { uuid: "7", label: "Entry 7" },  
  { uuid: "8", label: "Entry 8" },  
  { uuid: "9", label: "Entry 9" },  
  { uuid: "10", label: "Entry 10" },  
] );
```

:::details Addressing custom components
This example is intentionally picking one custom component by its name and another one by referring to its implementation. Using names requires named component to be globally registered on client side, but its name can be part of the data e.g. fetched from a server. In opposition to that, referring to a component's implementation works with local components. 
:::

This would result in a list like this:

<DemoDraggable mode="mixed"  />


## Properties

### cardComponent

This optional property selects a custom component for rendering all cards of list by default. It can be the name of a globally registered component or a reference to a locally available component. The component gets either object of list in **cards** as property **data**.

When omitted, cards are displayed as simple frames with a label by default. See the simple example above for illustration.

#### Implementing custom card components

A custom card component has to comply with some conventions:

- It has to [expose a DOM element](https://vuejs.org/api/sfc-script-setup.html#defineexpose) as `element` so that the embedding `DraggableCard` component is capable of discovering its extents on starting to drag it.

- It should expect a property `data` providing the record found as item in the list's [`cards`](#cards) property describing the content of resulting card.

- It may respond to boolean properties `draggable`, `dragging` and `floating` e.g. by applying different styles.

  * `draggable` indicates, whether this card can be dragged or not.
  * `dragging` indicates, if __any card of the list__ is currently dragged by the user.
  * `floating` indicates, if __this card__ is currently dragged by the user.

An example for a custom card component could look like this:

```vue
<script setup>
import { ref } from "vue";

defineProps( {
    data: Object,
    floating: Boolean,
    draggable: Boolean,
    dragging: Boolean,
} );

// expose reference on resulting component's root element as `element`
const element = ref();
defineExpose({element});
</script>

<template>
    <div class="my-card" :class="{locked: data.locked, floating}" ref="element">
        <h3>My Card</h3>
        <div>
            <label>UUID:</label>
            <span>{{ data.uuid }}</span>
        </div>
        <div>
            <label>Label:</label>
            <span>{{ data.label }}</span>
        </div>
    </div>
</template>

<style scoped lang="scss">
  // put your card's styling here
</style>
```

Assuming this component has been registered globally in context of your application as `MyCustomCard`, the property `cardComponent` of your `DraggableCardsList` would be the string `"MyCardComponent"`:

```html
<DraggableCardsList
  card-component="MyCardComponent"
  :drop-zone-size="20"
  :cards="items"
  @move-card="moveCard"
></DraggableCardsList>
```

To use it only on some cards, add property `cardComponent` to either record of list provided in [`cards`](#cards) property of `DraggableCardsList`:

```js
const items = ref( [
  { uuid: "1", label: "Entry 1" },  
  { uuid: "2", label: "Entry 2" },  
  { uuid: "3", label: "Entry 3", cardComponent: "MyCardComponent" },  
  { uuid: "4", label: "Entry 4" },  
  { uuid: "5", label: "Entry 5", cardComponent: "MyCardComponent" },  
  { uuid: "6", label: "Entry 6", locked: true },  
  { uuid: "7", label: "Entry 7" },  
  { uuid: "8", label: "Entry 8" },  
  { uuid: "9", label: "Entry 9" },  
  { uuid: "10", label: "Entry 10" },  
] );
```

### cards

This is a required array of objects with each object describing another card of list. Every listed object is expected to have these properties at least:

| Property      | Type    | Description                                                            |
|---------------|---------|------------------------------------------------------------------------|
| uuid          | string  | unique identifier of card                                              |
| label         | string  | text to display on a card (unless using custom component)              |
| cardComponent | string  | name of custom card component for rendering this object |
| locked        | boolean | prevents the card from being dragged when set                          |

### dropZoneSize

This optional numeric property controls width/height in pixels of gaps between two adjacent cards.

### scrollSpeed

This property is forwarded to either embedded instance of [DraggableCard](draggable-card.md#scrollspeed) component to control the speed of scrolling list while holding a card over edges of the list's viewport. It is given as the number of pixels to scroll by per second.

## Events

### move-card

This event is emitted to request single card of list to be moved to a different position. It expects an update of items listed in `cards` property in response to that.

The event is emitted with the card's old index into the list and its new index as arguments.
