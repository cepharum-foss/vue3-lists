---
outline:
  level: [2,3]
---
# Getting started

## Installation

In a consuming project, install this dependency by running

```sh
npm i -D @cepharum/vue3-lists @cepharum/vue3-i18n
```

This will install another library providing i18n support which is used to localize some components of this library.

Make sure all additional peer dependencies have been set up as well. 

## Integration

Some components of this library are built with web components of separately distributed [Shoelace](https://shoelace.style) library. For integrating this library with your project, that project needs to integrate Shoelace library of proper version prior to integrating this library.

### Vue3

First, set up Shoelace according to [the official guide for Vue integration](https://shoelace.style/frameworks/vue#installation). This usually involves your project's **main.js** resulting in something like this:

:::code-group
```javascript [main.js]
import { createApp } from "vue";

import App from "./App.vue";

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path.js"; // [!code ++]
import "@shoelace-style/shoelace/dist/themes/light.css"; // [!code ++]
import "@shoelace-style/shoelace/dist/themes/dark.css"; // [!code ++]

setBasePath( import.meta.env.BASE_URL + "shoelace/" ); // [!code ++]

const app = createApp( App );

// TODO: set up the application here as usual

app.mount( "#app" );
```
:::

Next, integrate this library by importing its custom styles and invoke a method for picking the Shoelace components it is potentially using.

Importing components works asynchronously, thus wait for it to complete prior to eventually setting up your application to prevent issues due to race conditions.

:::code-group
```javascript [main.js]
import { createApp } from "vue";

import App from "./App.vue";

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path.js";
import "@shoelace-style/shoelace/dist/themes/light.css";
import "@shoelace-style/shoelace/dist/themes/dark.css";

import { setup } from "@cepharum/vue3-lists"; // [!code ++]
import "@cepharum/vue3-lists/dist/style.css"; // [!code ++]

setBasePath( import.meta.env.BASE_URL + "shoelace/" );

setup().then( () => { // [!code ++]
  const app = createApp( App );

  // TODO: set up the application here as usual
  
  app.mount( "#app" );
} ); // [!code ++]
```
:::

::::details Don't forget to set up icons!
Don't forget to copy icons of Shoelace into your project's output folder when building your project. Here is one way to achieve that:

1. Install a plugin for Vite by running

   ```shell
   npm i -D vite-plugin-static-copy
   ```

2. In your project's **vite.config.js** file, this plugin must be registered like this:

   :::code-group
   ```javascript [vite.config.js]
   import { defineConfig } from "vite";
   import { fileURLToPath, URL } from "node:url";
   
   import vue from "@vitejs/plugin-vue";
   import { viteStaticCopy } from "vite-plugin-static-copy"; // [!code ++]
   
   export default defineConfig( {
     plugins: [
       vue(),
       viteStaticCopy( { // [!code ++]
         targets: [ // [!code ++]
           { // [!code ++]
             src: "node_modules/@shoelace-style/shoelace/dist/assets/", // [!code ++]
             dest: "shoelace/", // [!code ++]
           } // [!code ++]
         ] // [!code ++]
       } ), // [!code ++]
     ],
     build: {
       outDir: "server/public"
     }
   } );
   ```
   :::
::::

### VitePress

[VitePress](https://vitepress.dev/) is based on [Vue3](https://vuejs.org/). Integration works mostly similar to Vue3 [as described above](#vue3), but files mentioned there aren't available. Adjustments are made in different files.

First, set up Shoelace by installing it as a dependency:

```shell
npm i -D @shoelace-style/shoelace
```

For there is no **main.js** file to edit, you need to either [create a custom theme](https://vitepress.dev/guide/custom-theme) or [extend the default one](https://vitepress.dev/guide/extending-default-theme). Either way, you'll end up with a theme definition file to be adjusted next.

Integrate Shoelace there:

::: code-group
```javascript [.vitepress/theme/index.js]
import DefaultTheme from 'vitepress/theme'

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path"; // [!code ++]
import "@shoelace-style/shoelace/dist/themes/light.css"; // [!code ++]
import "@shoelace-style/shoelace/dist/themes/dark.css"; // [!code ++]

setBasePath( import.meta.env.BASE_URL + "shoelace/" ); // [!code ++]

export default {
  extends: DefaultTheme,
  enhanceApp({ app }) {
    // TODO customize your theme here, e.g. by registering global components
  }
}
```
:::

Next, integrate this library by importing its custom styles and invoke a method for picking the Shoelace components it is potentially using.

Importing components works asynchronously, thus wait for it to complete. The `enhanceApp()` method supports async/await syntax or returning a promise for that:

:::code-group []
```javascript [.vitepress/theme/index.js]
import DefaultTheme from 'vitepress/theme'

import { setBasePath } from "@shoelace-style/shoelace/dist/utilities/base-path";
import "@shoelace-style/shoelace/dist/themes/light.css";
import "@shoelace-style/shoelace/dist/themes/dark.css";

import { setup } from "@cepharum/vue3-lists"; // [!code ++]
import "@cepharum/vue3-lists/dist/style.css"; // [!code ++]

setBasePath( import.meta.env.BASE_URL + "shoelace/" );

export default {
  extends: DefaultTheme,
  enhanceApp({ app }) { // [!code --]
  async enhanceApp({ app }) { // [!code ++]
    await setup( true ) // [!code ++]

    // TODO customize your theme here, e.g. by registering global components
  }
}
```
:::

Method `setup()` is invoked with argument `true` here to use current locale of `document` on looking up translations. This way components use proper locale in an internationalized VitePress project.

::::details Don't forget to set up icons!
Don't forget to copy icons of Shoelace into your project's output folder when building the site. Here is one way to achieve that:

1. Install a plugin for Vite by running

   ```shell
   npm i -D vite-plugin-static-copy
   ```

2. There is no **vite.config.js** file in a VitePress project. However, customizing Vite's configuration is [supported as part of its configuration](https://vitepress.dev/reference/site-config#vite). Register the plugin there like this:

   :::code-group 
   ```javascript [.vitepress/config.js]
   import {viteStaticCopy} from "vite-plugin-static-copy"; // [!code ++]

   export default {
     lang: 'en-US',
     title: 'VitePress',
     description: 'Vite & Vue powered static site generator.',
     ...
   
     vite: { // [!code ++]
       plugins: [ // [!code ++]
         viteStaticCopy( { // [!code ++]
           targets: [ // [!code ++]
             { // [!code ++]
               src: "node_modules/@shoelace-style/shoelace/dist/assets/", // [!code ++]
               dest: "shoelace/", // [!code ++]
             } // [!code ++]
           ] // [!code ++]
         } ), // [!code ++]
       ], // [!code ++]
     }, // [!code ++]
   }
   ```

   ```javascript [docs/.vitepress/config.js]
   import {viteStaticCopy} from "vite-plugin-static-copy"; // [!code ++]

   export default {
     lang: 'en-US',
     title: 'VitePress',
     description: 'Vite & Vue powered static site generator.',
     ...
   
     vite: { // [!code ++]
       plugins: [ // [!code ++]
         viteStaticCopy( { // [!code ++]
           targets: [ // [!code ++]
             { // [!code ++]
               src: "../node_modules/@shoelace-style/shoelace/dist/assets/", // [!code ++]
               dest: "shoelace/", // [!code ++]
             } // [!code ++]
           ] // [!code ++]
         } ), // [!code ++]
       ], // [!code ++]
     }, // [!code ++]
   }
   ```

   :::

   Choose the variant matching [your setup](https://vitepress.dev/guide/getting-started#file-structure):
   
   1. When running VitePress in a standalone project, use configuration in file **.vitepress/config.js**.

   2. When running VitePress in the sub-folder of another project, use configuration in file **docs/.vitepress/config.js**.
   
   The essential difference is in option `src` of injected plugin which is related to the folder containing the VitePress files trying to select dependencies managed in context of a potentially containing project. When putting VitePress in a deeper folder of a containing project you need to use `../` as many times as necessary to properly address the `node_modules` folder in root folder of that containing project.
::::
