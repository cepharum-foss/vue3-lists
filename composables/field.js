import { computed, ref, toRefs, toValue, unref, watch, watchEffect } from "vue";
import { clone, compare, isValidListManager } from "../lib/helper";

export const collectFieldOptions = ( collection, option, lookup ) => {
	if ( lookup != null && option.value !== lookup ) {
		return;
	}

	collection.push( option );
};

export const useFieldOptions = ( rawOptions, lookUp ) => {
	const _options = typeof rawOptions === "string" ?
		rawOptions.trim().split( /\s*,\s*/ ) : rawOptions;

	const copy = [];

	if ( Array.isArray( _options ) ) {
		for ( const option of _options ) {
			if ( typeof option === "string" && option.trim() !== "" ) {
				collectFieldOptions( copy, { value: option, label: option }, lookUp );
			} else if ( Array.isArray( option ) && option.length === 2 ) {
				collectFieldOptions( copy, { value: option[0], label: option[1] }, lookUp );
			} else if ( option && typeof option === "object" ) {
				collectFieldOptions( copy, { ...option }, lookUp );
			}

			if ( lookUp != null && copy.length > 0 ) break;
		}
	} else if ( _options instanceof Map ) {
		for ( const [ value, option ] of _options.entries() ) {
			if ( typeof option === "string" && option.trim() !== "" ) {
				collectFieldOptions( copy, { value, label: option }, lookUp );
			} else if ( option && typeof option === "object" ) {
				collectFieldOptions( copy, { value, ...option }, lookUp );
			}

			if ( lookUp != null && copy.length > 0 ) break;
		}
	} else if ( _options && typeof _options === "object" ) {
		for ( const value of Object.keys( _options ) ) {
			const option = _options[value];

			if ( typeof option === "string" && option.trim() !== "" ) {
				collectFieldOptions( copy, { value, label: option }, lookUp );
			} else if ( option && typeof option === "object" ) {
				collectFieldOptions( copy, { value, ...option }, lookUp );
			}

			if ( lookUp != null && copy.length > 0 ) break;
		}
	}

	if ( lookUp != null ) {
		return copy[0];
	}

	return copy
		.filter( i => typeof unref( i ).label === "string" )
		.sort( ( a, b ) => {
			const l = unref( a );
			const r = unref( b );

			return l.label.localeCompare( r.label, undefined, { numeric: true, sensitivity: "base" } );
		} );
};

/**
 * Implements common behavior of field controls for generic list editor.
 *
 * @param {Ref<HTMLElement>} field reference on field in DOM (used for native checking)
 * @param {Object<string, any>} props properties of component
 * @param {function():void} emit callback for emitting events
 * @returns {FieldComposable} API of common behavior of field controls
 */
export function useField( field, props, emit ) {
	const original = ref();

	/**
     * Takes snapshot of current value for detecting changes.
     *
     * @returns {void}
     */
	function snapshot() {
		original.value = clone( toValue( props.modelValue ) );
	}

	snapshot();

	/**
     * Validates provided value in context of current field.
     *
     * @param {any} value value to be assessed
     * @returns {void}
     */
	function validate( value ) {
		let errors = new Map();

		if ( typeof props.schema.validate === "function" ) {
			try {
				errors = props.schema.validate( value ) || errors;

				if ( typeof errors === "object" && !( errors instanceof Map ) ) {
					errors = new Map( Object.keys( errors ).map( key => [ key, true ] ) );
				}
			} catch ( cause ) {
				console.error( cause );
				errors[cause.message] = true;
			}
		}

		if ( props.schema.required && !String( value ?? "" ).trim() ) {
			errors.set( "@vue3-lists.GENERIC_LIST.EDITOR.VALIDATION.required", true );
		}

		if ( field.value && !field.value.checkValidity() ) {
			errors.set( "@vue3-lists.GENERIC_LIST.EDITOR.VALIDATION.native", true );
		}

		emit( "update:errors", Array.from( errors.keys() ) );
	}

	const changed = computed( () => !compare( original.value, toValue( props.modelValue ), false ) );

	/**
     * Report change of changed-status as part of related two-way binding.
     */
	watch( changed, now => {
		emit( "update:changed", now );
	} );

	const { touched, modelValue, version, schema } = toRefs( props );

	const rawOptions = ref();

	const loading = ref( true );

	watchEffect( async() => {
		loading.value = true;

		try {
			rawOptions.value = await schema.value.options;
		} finally {
			loading.value = false;
		}
	} );

	const options = computed( () => useFieldOptions( rawOptions.value ) );

	/**
     * Revalidates field when touched.
     */
	watch( touched, ( now, before ) => {
		if ( now && !before ) {
			validate( modelValue.value );
		}
	} );

	/**
     * Takes snapshot when version of field's value is changed.
     */
	watch( version, snapshot );

	/**
     * Re-validates updated value of field.
     */
	watch( modelValue, now => {
		if ( touched.value ) {
			validate( now );
		}
	} );

	/**
     * Responds to user having changed field's value as provided.
     *
     * @param {any} value user-adjusted value of field
     * @returns {void}
     */
	const onInput = value => {
		emit( "update:modelValue", value );

		if ( !touched.value ) {
			emit( "update:touched", true );
		}
	};

	const normalizedType = computed( () => ( props.schema.fieldType || props.schema.type || "text" ).toLowerCase().trim() );

	const fieldType = computed( () => {
		switch ( normalizedType.value ) {
			case "number" :
			case "email" :
			case "url" :
			case "tel" :
			case "phone" :
			default :
				return "text";

			case "secret" :
			case "password" :
				return "password";

			case "select" :
			case "enum" :
				return "select";

			case "check" :
			case "checkbox" :
			case "flag" :
			case "mark" :
				return "checkbox";

			case "uuid" :
				return "related";
		}
	} );

	const textInputMode = computed( () => {
		switch ( normalizedType.value ) {
			case "number" :
				return "decimal";

			case "email" :
				return "email";

			case "url" :
				return "url";

			case "tel" :
			case "phone" :
				return "tel";

			default :
				return "text";
		}
	} );

	const block = event => {
		if ( event.type === "keydown" && event.key !== " " ) {
			return;
		}

		event.preventDefault();
	};

	watchEffect( () => {
		if ( field.value ) {
			if ( schema.value.readonly ) {
				field.value.addEventListener( "click", block, { capture: true } );
				field.value.addEventListener( "keydown", block, { capture: true } );
			} else {
				field.value.removeEventListener( "click", block, { capture: true } );
				field.value.removeEventListener( "keydown", block, { capture: true } );
			}
		}
	} );

	watch( field, ( now, before ) => {
		if ( before ) {
			before.removeEventListener( "click", block, { capture: true } );
			before.removeEventListener( "keydown", block, { capture: true } );
		}
	} );

	return {
		snapshot,
		validate: () => validate( props.modelValue ),
		onInput,
		options,
		schema,
		fieldType,
		textInputMode,
		loading,
	};
}

export const events = [ "update:modelValue", "update:errors", "update:touched", "update:changed" ];

/** @type {FieldCommonProperties} */
export const properties = {
	schema: {
		type: Object,
		required: true,
	},
	modelValue: {
		type: [ Object, String, Number, Boolean ],
	},
	errors: {
		type: Array,
		default: () => [],
		validator: value => Array.isArray( value ) && value.every( item => typeof item === "string" && item.trim() ),
	},
	manager: {
		type: Object,
		required: true,
		validator: isValidListManager,
	},
	touched: {
		type: Boolean,
	},
	changed: {
		type: Boolean,
	},
	version: {
		type: Number,
	},
	label: {
		type: String,
	}
};
