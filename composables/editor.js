import { computed, nextTick, ref, toRefs, toValue, watch, watchEffect } from "vue";
import { isValidListManager } from "../lib/helper";

/**
 * Provides API commonly useful for implementing custom editor components.
 *
 * @param {Object<string,any>} props actual properties of consuming component instance
 * @param {function(eventName:string, ...args):void} emit event dispatcher of consuming component instance
 * @param {EditorComposableOptions} options customizations for the composable
 * @returns {EditorComposable} API for implementing custom editor component
 */
export function useEditor( props, emit, options = {} ) {
	/**
	 * Lists properties of item to present fields for.
	 *
	 * @type {Ref<Object<string,FieldDefinition>>}
	 */
	const fields = ref( [] );

	const fieldsByName = ref( {} );

	/**
	 * Tracks state of either field.
	 *
	 * @type {Ref<Object<string,FieldState>>}
	 */
	const states = ref( {} );

	/**
	 * Locally computes whether all fields are currently valid or not.
	 *
	 * @type {ComputedRef<this is any[]>}
	 */
	const isValid = computed( () => Object.values( states.value ).every( state => !state.errors?.length ) );

	/**
	 * Locally computes if any field has unsaved changes.
	 *
	 * @type {ComputedRef<boolean>}
	 */
	const hasChanged = computed( () => Object.values( states.value ).some( state => state.changed ) );

	/**
	 * Caches definition of properties to actually use for deriving definition
	 * of fields to present as part of editor.
	 *
	 * @type {ComputedRef<Object<string,PropertyDefinition>>}
	 */
	const properties = computed( () => options.properties || props.manager.schema?.props || {} );

	/**
	 * Indicates if properties must be marked explicitly to be included or marked to
	 * be excluded.
	 *
	 * @type {ComputedRef<boolean>}
	 */
	const includeMarked = computed( () => Object.values( properties.value ).some( p => p?.editable ) );

	/**
	 * Lists names of properties to be rendered as field of editor in proper order.
	 *
	 * @type {ComputedRef<string[]>}
	 */
	const fieldNames = computed( () => {
		return Object.keys( properties.value )
			.filter( name => {
				const property = properties.value[name];

				if ( property.hidden || ( includeMarked.value ? !property.editable : false ) ) {
					// don't consider this proper e.g. on rendering editor
					return false;
				}

				if ( property.type === "uuid" && !property.component && !property.fieldType && !property.relatedManager ) {
					return false;
				}

				return true;
			} )
			.sort( ( l, r ) => ( properties.value[l]?.fieldIndex - properties.value[r]?.fieldIndex ) || 0 );
	} );

	/**
	 * Computes fields to present based on latest schema definition describing
	 * properties of current item.
	 */
	watchEffect( () => {
		const compiledFields = [];
		const compiledFieldsByName = {};

		for ( const name of fieldNames.value ) {
			states.value[name] ??= {
				value: undefined,
				errors: [],
				changed: false,
				touched: false,
				version: 0,
			};

			const compiledField = {
				...properties.value[name],
				name,
			};

			compiledFields.push( compiledField );
			compiledFieldsByName[name] = compiledField;
		}

		fields.value = compiledFields;
		fieldsByName.value = compiledFieldsByName;
	} );

	/**
	 * Emits on change of fields being valid or not.
	 */
	watchEffect( () => {
		emit( "isValid", isValid.value );
	} );

	/**
	 * Emits on change of fields having some unsaved changes or not.
	 */
	watchEffect( () => {
		emit( "hasChanged", hasChanged.value );
	} );

	/**
	 * Resets state of named field using provided value as new original value.
	 *
	 * @param {string} name name of field to reset
	 * @param {any} value value of field to use as its new original value
	 * @returns {void}
	 */
	const resetState = ( name, value = undefined ) => {
		const state = states.value[name];

		state.value = value;
		state.version++;
		state.touched = false;
		state.changed = false;
	};

	/**
	 * Fetches record of selected item from database and assigns fetched properties
	 * to fields.
	 *
	 * @returns {Promise<void>} promises properties fetched
	 */
	const read = async() => {
		emit( "loading" );

		try {
			const values = await ( options.reader || props.manager.read )( props.item ) || {};

			for ( const name of fieldNames.value ) {
				resetState( name, values[name] );
			}

			emit( "loaded", false );
		} catch ( cause ) {
			console.error( cause );
			emit( "loaded", cause );
		}
	};

	/**
	 * Responds to user clicking button or pressing Enter for saving changes.
	 *
	 * @returns {Promise<void>} promises processing is done
	 */
	const write = async() => {
		for ( const name of fieldNames.value ) {
			states.value[name].touched = true;
		}

		await nextTick();

		if ( isValid.value ) {
			const values = {};

			for ( const name of fieldNames.value ) {
				values[name] = toValue( states.value[name].value );
			}

			if ( options.writer ) {
				await options.writer( props.item || undefined, values );
			} else if ( props.item ) {
				await props.manager.update( props.item, values );
			} else {
				await props.manager.create( values );
			}
		} else {
			// eslint-disable-next-line no-throw-literal
			throw "@vue3-lists.GENERIC_LIST.EDITOR.VALIDATION.toast";
		}
	};

	const { item } = toRefs( props );

	/**
	 * Responds to change of item to edit fetching its data from backend or by
	 * resetting all fields in case no particular item is given.
	 */
	watch( item, now => {
		if ( now ) {
			read();
		} else {
			for ( const name of fieldNames.value ) {
				resetState( name );
			}
		}
	}, { immediate: true } );

	return {
		fields: computed( () => fields.value ),
		fieldsByName: computed( () => fieldsByName.value ),
		states,
		isNew: computed( () => !props.item ),
		isValid,
		hasChanged,
		includeMarked,
		fieldNames,
		resetState,
		read,
		write
	};
}

/**
 * Define common properties of editor components.
 *
 * @type {manager: Object<type, string>, item: Object, i18nPrefix: String}
 */
export const properties = {
	// exposes manager API for interacting with the data source
	manager: {
		type: Object,
		required: true,
		validator: isValidListManager,
	},
	// addresses item to edit, if nullish, new item is created
	item: Object,
	i18nPrefix: String,
};

/**
 * Lists names of events commonly supported by editor components.
 *
 * @type {string[]}
 */
export const events = [
	// emitted on requesting data for editing item from backend
	"loading",
	// emitted when finished loading data for editing (provides error when loading finished on failure)
	"loaded",
	// emitted when content switches its state of being valid or not
	"isValid",
	// emitted when content switches its state of having unsaved changes or not
	"hasChanged",
];
