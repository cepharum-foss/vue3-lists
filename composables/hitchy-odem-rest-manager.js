import { ref } from "vue";
import { merge } from "../lib/helper.js";

const schemataCache = {};

/**
 * Creates manager for use with generic list which is binding some remote model
 * implemented with Hitchy's ODM via REST API.
 *
 * @see https://odem.hitchy.org/
 * @see https://www.npmjs.com/package/@hitchy/plugin-odem-rest
 *
 * @param {string} baseUrl URL addressing the model's collection
 * @param {boolean} schema if true, schema is fetched from backend using custom request
 * @param {Type<Cepharum.Vue3Lists.Item>} itemClass class that is used to create Item Obj
 * @param {string} queryUrl url to use instead of baseUrl for query request
 * @param {string} readUrl url to use instead of baseUrl for read request
 * @param {Object} schemaOverlay partial schema to be deeply merged with fetched schema
 * @returns {Partial<ListManager>} parts of list manager related to accessing datasource based on Hitchy's ODM via REST API
 */
export async function useHitchyOdemRestManager( baseUrl, {
	schema = true,
	itemClass = null,
	queryUrl = null,
	readUrl = null,
	schemaOverlay = {}
} = {} ) {
	const items = ref( [] );
	const count = ref( 0 );

	const url = baseUrl.replace( /\/+$/, "" );

	let _schema = {};

	if ( schema ) {
		if ( !schemataCache[url] ) {
			// fetch schema from backend
			schemataCache[url] = Promise.resolve()
				.then( async() => {
					const res = await fetch( url + "/.schema" );
					const data = await res.json();

					if ( res.status !== 200 ) {
						throw new Error( `fetching schema failed: ${data.error || "unknown error"}` );
					}

					return data;
				} );
		}

		_schema = merge( await schemataCache[url], schemaOverlay );
	}

	const read = async item => {
		if ( !item?.uuid ) {
			throw new Error( "missing UUID of item to read" );
		}

		let _url = readUrl || url;

		_url = _url.indexOf( ":uuid" ) < 0 ? _url + "/" + item.uuid : _url.replace( /:uuid/g, item.uuid );

		const response = await fetch( _url, {
			method: "GET",
			headers: {
				Accept: "application/json",
			},
		} );

		if ( response.status !== 200 ) {
			throw new Error( `fetching item ${item.uuid} failed with ${response.status}: ${response.statusText}` );
		}

		const record = await response.json();

		if ( itemClass ) {
			return Object.assign( Object.create( itemClass.prototype ), record );
		}

		return record;
	};

	let queryVersion = 0;

	/** @type ListManager */
	return {
		items,
		count,
		async query( filter = {}, options = {} ) {
			const parameters = {};
			let clientSideFilter = false;
			let sliced = false;

			if ( options.sortBy || this.defaultSorting ) {
				parameters.sortBy = options.sortBy || this.defaultSorting.sortBy;

				const sortAscending = options.sortBy ? options.sortAscending : this.defaultSorting.sortAscending;
				if ( sortAscending === false ) {
					parameters.descending = 1;
				}
			}

			const q = [];

			for ( const property of Object.keys( filter || {} ) ) {
				if ( property === "*" ) {
					console.warn( "[vue3-lists] support for server-side filtering is still missing ... have to fetch all items to filter client-side" );
					clientSideFilter = true;
				} else {
					const value = filter[property];

					if ( Array.isArray( value ) && value.length === 2 ) {
						q.push( { in: [...value] } );
					} else if ( typeof value === "object" ) {
						q.push( value );
					} else {
						q.push( { eq: { [property]: String( value ?? "" ) } } );
					}
				}
			}


			if ( !clientSideFilter ) {
				if ( q.length > 0 ) {
					parameters.query = JSON.stringify( { and: q } );
				}

				if ( options.offset > 0 ) {
					parameters.offset = parseInt( options.offset ) || 0;
					sliced = true;
				}

				if ( options.limit > 0 ) {
					parameters.limit = parseInt( options.limit ) || 1000000;
					sliced = true;
				}

				if ( sliced ) {
					parameters.count = 1;
				}
			}

			const _url = ( queryUrl || url ) + "?" +
				Object.keys( parameters )
					.map( name => encodeURIComponent( name ) + "=" + encodeURIComponent( parameters[name] ) )
					.join( "&" );

			const response = await fetch( _url, {
				method: "GET",
				headers: {
					Accept: "application/json",
				},
			} );

			if ( response.status !== 200 ) {
				throw new Error( `query failed with status ${response.status}: ${response.statusText}` );
			}

			const myVersion = ++queryVersion;

			let data = await response.json();
			let records;

			if ( Array.isArray( data ) ) {
				if ( clientSideFilter ) {
					const search = String( filter["*"] ?? "" ).trim().toLowerCase().replace( /\s+/g, " " );
					const matching = new Array( data.length );
					let write = 0;

					for ( const item of data ) {
						let match = false;

						for ( const name of Object.keys( item || {} ) ) {
							if ( name !== "uuid" && item[name].toLowerCase().replace( /\s+/g, " " ).includes( search ) ) {
								match = true;
								break;
							}
						}

						if ( match ) {
							matching[write++] = item;
						}
					}

					matching.splice( write );
					data = matching;
				}

				count.value = data.length;

				if ( clientSideFilter ) {
					if ( options.offset > 0 ) {
						if ( options.limit > 0 ) {
							data = data.slice( options.offset, options.offset + options.limit );
						} else {
							data = data.slice( options.offset );
						}
					}
				}

				records = data;
			} else if ( data && Array.isArray( data.items ) ) {
				count.value = data.count ?? 0;
				records = data.items;
			}

			if ( itemClass ) {
				for ( let i = 0, length = records.length; i < length; i++ ) {
					records[i] = Object.assign( Object.create( itemClass.prototype ), records[i] );
				}
			}

			if ( myVersion === queryVersion ) {
				// there hasn't been another query sent concurrently
				items.value = records;

				return true;
			}

			return false;
		},
		async create( properties ) {
			const response = await fetch( url, {
				method: "POST",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify( properties ),
			} );

			if ( response.status !== 201 ) {
				throw new Error( `creating item in data source failed with ${response.status}: ${response.statusText}` );
			}

			// TODO fix in backend
			const location = response.headers.location;
			if ( location ) {
				const split = location.lastIndexOf( "/" );

				if ( location.slice( 0, split + 1 ) !== url + "/" ) {
					throw new Error( `unexpected redirection to created item's URL at ${location}` );
				}

				return { collectionUpdated: false, createdItem: { uuid: location.slice( split + 1 ) } };
			}
			const body = await response.json();
			if ( body.uuid ) {
				return { collectionUpdated: false, createdItem: { uuid: body.uuid } };
			}
			throw new Error( "unexpected response on creating new Item: missing location" );
		},
		read,
		async update( item, properties ) {
			const response = await fetch( url + "/" + item.uuid, {
				method: "PATCH",
				headers: {
					"content-type": "application/json"
				},
				body: JSON.stringify( properties ),
			} );

			if ( response.status !== 200 ) {
				throw new Error( `updating item in data source failed with ${response.status}: ${response.statusText}` );
			}

			return false;
		},
		async delete( itemsToDelete ) {
			const _items = Array.isArray( itemsToDelete ) ? itemsToDelete : itemsToDelete ? [itemsToDelete] : [];

			await Promise.all( _items.map( item => {
				return fetch( url + "/" + item.uuid, {
					method: "DELETE",
				} ).then( response => {
					if ( response.status !== 200 ) {
						throw new Error( `deleting item ${item.uuid} in data source failed with ${response.status}: ${response.statusText}` );
					}
				} );
			} ) );

			return false;
		},
		schema: _schema,
	};
}
