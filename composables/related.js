import {computed, ref, toRef, unref} from "vue";
import { usePromise } from "@cepharum/vue3-utils/dist/vue3-utils.js";

/**
 * Provides reactive information on a related field and its label.
 *
 * @param {Cepharum.Vue3Lists.Item} item some local item referring to a related one
 * @param {Cepharum.Vue3Lists.ListManager} manager manager of provided local item
 * @param {string|Ref<string>} property name of local item's property referring to related item to represent
 * @returns {Cepharum.Vue3Lists.ReactiveRelatedItem} reactive information on related item
 */
export function useRelated( item, manager, property ) {
	// extract ID of a related item as given in a selected property of current item
	const foreignKey = computed( () => unref( item )[unref( property )] );

	// derives reactive result of fetching selected foreign item
	const result = computed( () => {
		const _manager = unref( manager );
		const schema = unref( _manager.schema?.props[unref( property )] );
		const relatedManager = _manager.relatedManagers?.[schema?.relatedManager];

		if ( relatedManager && foreignKey.value ) {
			return usePromise( uuid => relatedManager.read( { uuid } ), foreignKey.value ).value;
		}

		return { record: null };
	} );

	// exposes fetched related item
	const related = computed( () => result.value?.record );

	// exposes error encountered on fetching related item
	const error = computed( () => result.value?.error );

	// derives ref exposing a related item's label (not some label itself!)
	const labelSource = computed( () => {
		const _item = related.value;

		if ( !_item ) {
			return ref( null );
		}

		return typeof _item.getLabel === "function" ? toRef( _item.getLabel() ) : ref( String( _item ) );
	} );

	// derives actual label from ref exposing it per related item
	const label = computed( () => labelSource.value.value );

	// controls visibility of local loading indicator
	const loading = computed( () => result.value == null );

	return {
		foreignKey,
		error,
		item: related,
		label,
		loading,
	};
}
